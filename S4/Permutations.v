(************* CONTEXT EQUIVALENCE *****************)
(* The notion of context equivalence in this system is defined 
   using permutations.
   Two contexts are equivalent iff one is permutaion of the other.
   A permutation is defined using a non-deterministic proposition for 
   insertion in a given context.
 *)

Require Import Coq.Program.Equality.
Require Import ModalLogic.
Require Import Context.
Require Import HK4.


(* Context permutation *)
Inductive Insert: Formula -> ctx -> ctx -> Prop:=
  | insLast : forall (A: Formula) (G: ctx), Insert A G (G,A)
  | insRec  : forall (A B: Formula) (G G': ctx), 
              Insert A G G' -> Insert A (G,B) (G',B).

Global Hint Constructors Insert : ctxPerm.

Inductive Perm: ctx -> ctx -> Prop:=
  | permEmpty : Perm empty empty
  | permRec   : forall (A : Formula) (G Gp G': ctx), 
                Perm G Gp -> Insert A Gp G' -> Perm (G,A) G'.

Global Hint Constructors Perm : ctxPerm.

(**** permutation properties ***************)

Lemma perm_refl: forall (G:ctx), Perm G G.
Proof.
intro.
induction G; intuition.
econstructor 2.
- exact IHG.
- constructor.
Qed.

Global Hint Resolve perm_refl : ctxPerm.


Lemma perm_swap: 
  forall (G:ctx) (A B: Formula), Perm ((G,A),B) ((G,B),A).
Proof.
intros.
econstructor 2.
- apply perm_refl.
- intuition.
Qed.

Global Hint Resolve perm_swap : ctxPerm.


Lemma perm_snoc: 
  forall (G G':ctx) (A: Formula), Perm G G' -> Perm (G,A) (G',A).
Proof.
intros.
dependent induction G; inversion H.
- eapply permRec.
  intuition.
  apply insLast.
- econstructor.
  -- exact H.
  -- intuition. (* apply insLast.  *)
Qed.

Global Hint Resolve perm_snoc : ctxPerm.


(* Induction principle for permutations *)
Theorem Perm_ind_pairs: 
  forall (P: ctx -> ctx -> Prop), 
  P empty empty ->
  (forall A G Gp G', Perm G Gp -> P G Gp -> Insert A Gp G' -> P (G,A) G') -> 
  forall D D', Perm D D' -> P D D'.
Proof.
intros P He Hs D.
induction D; intros; inversion H.
- exact He.
- apply (Hs f D Gp).
  -- exact H2.
  -- apply IHD; assumption.
  -- assumption.
Qed.



Lemma perm_conc: forall (G'' G' G:ctx), Perm G G' -> Perm (G;G'') (G';G'').
Proof.
intro.
induction G''.
intuition.
intros.
simpl.
apply perm_snoc.
intuition.
Qed.

Global Hint Resolve perm_conc : ctxPerm.


(*
Lemma PermAppInv: 
  forall (G1 G2 G3 G4:ctx) (A:Formula),
  Perm ((G1,A);G2) ((G3,A);G4) -> Perm (G1;G2) (G3;G4).
Proof.
intros.
(*
dependent induction G2.
simpl.
simpl in H.
eapply permRec in H.


- admit.
- eapply IHPerm.


 set(P G G' := forall A G1 G2 G3 G4, 
               G = (G1,A);G2 -> G' = (G3,A);G4 -> Perm (G1;G2) (G3;G4)).
intros.
(* eapply (P ((G1, A); G2) ((G3, A); G4) A G1 G2 G3 G4). *)

cut (forall D D', Perm D D' -> P D D').
intros.

*)

Admitted.


Lemma PermSnocV: forall (G' G:ctx) (A:Formula), 
Perm (G,A) (G',A) -> Perm G G'.
Proof.
intros. 
apply (PermAppInv G empty G' empty A).
auto.
Qed.
*)

Lemma insert_twice:
  forall (A:Formula) (G Ga:ctx), Insert A G Ga -> 
  forall (B:Formula) (Gab:ctx), Insert B Ga Gab ->
  exists Gb, Insert B G Gb  /\ Insert A Gb Gab.
Proof. 
intros a G Ga H.
dependent induction H; intros.
- inversion H.
  + exists (G,B).
    split.
    -- apply insLast.
    -- apply insRec. apply insLast.
  + exists G'; intuition.
- inversion H0.
  + exists((G,B),B0).
    split.
    -- apply insLast.
    -- apply insRec. apply insRec. assumption.
  + apply IHInsert in H5.
    destruct H5. destruct H5.
    exists (x,B).
    split.
    -- repeat (apply insRec).
       assumption.
    -- apply insRec. assumption.
Qed.

Global Hint Resolve insert_twice : ctxPerm.


Lemma insert_perm:
  forall (G Ga: ctx) (A: Formula), Insert A G Ga -> 
  forall (Gp:ctx), Perm Ga Gp -> exists (D:ctx), Insert A D Gp /\ Perm G D.
Proof.
intros G Ga A H'.
dependent induction H'; intros.
- inversion H.
  exists Gp0.
  split; assumption.
- inversion H.
  apply IHH' in H2.
  destruct H2. destruct H2.
  apply (insert_twice _ _ _ H2) in H4.
  destruct H4. destruct H4.
  exists x0.
  split.
  assumption.
  econstructor.
  exact H5.
  exact H4.
Qed.

Global Hint Resolve insert_perm : ctxPerm.



(************ Context Permutations and derivability in HK4 **********)
Lemma insert_deriv: 
  forall (A: Formula) (G Gi:ctx),
  Insert A G Gi -> forall (B:Formula), G,A |- B -> Gi |- B.
Proof.
intros A G Gi Hi.
induction Hi;auto.
intros.
apply detachment.
apply IHHi.
auto with HK4.
Qed.
(* MANUAL PROOF:
  change ((G,B),A) with ((G,B);(empty,A)) in H.
  apply T2_deductionTh_genPremise in H.
  simpl in H.
  assumption.
*)

(** OLD MANUAL PROOF:
intros A G Gi Hi.
induction Hi.
- intuition.
- intros.
apply premise_to_left in H.
apply T5_reverseDT.
apply IHHi.
apply premise_to_left.
apply T2_deductionTh.
assumption.
Qed.
**)

(* Hint Resolve insert_deriv. *)


(* The following proposition should hold for any deductive system *)
Proposition perm_deriv: 
  forall (G G':ctx), Perm G G' -> forall (A:Formula), G |- A -> G' |- A.
Proof.
intros G G' Hp.
induction Hp.
intuition.
intros.
eapply insert_deriv.
exact H.
apply detachment.
apply IHHp.
intuition. 
Qed.

(* Hint Resolve perm_deriv. *)


Lemma perm_trans: 
  forall (G1 G2:ctx), Perm G1 G2 -> forall (G3: ctx), Perm G2 G3 -> Perm G1 G3.
Proof.
intros G1 G2 H.
induction H.
+ auto.
+ intros.
  inversion H0.
- rewrite <- H4 in H1.
  inversion H1.
  apply IHPerm in H7.
  econstructor.
  exact H7.
  exact H9.
- cut(exists (D:ctx), Insert A D G3 /\ Perm Gp D) ; intros.
  destruct H6.
  destruct H6.
  econstructor.
  2:
  exact H6.
  apply IHPerm in H7.
  assumption.
  eapply insert_perm.
  2:
  exact H1.
  exact H0.
Qed.

(* Hint Resolve perm_trans. *)



Lemma perm_insert: 
  forall (G' G:ctx) (A:Formula), Insert A G G' -> Perm G' (G,A).
Proof.
intros.
induction H.
intuition.
assert (Perm ((G,A),B) ((G,B),A)) ; intuition.
eapply perm_snoc in IHInsert.
eapply perm_trans.
exact IHInsert.
intuition.
Qed.

(* Hint Resolve perm_insert. *)



Lemma perm_symm: forall (G' G:ctx), Perm G G' -> Perm G' G.
Proof.
intros.
induction H.
intuition.
inversion H0.
intuition.
clear H2.
apply perm_insert in H1.
apply (perm_snoc _ _ B) in H1.
assert (Perm ((G0,A),B) ((G0,B),A)).
intuition.
eapply perm_trans.
exact H1.
eapply perm_trans.
exact H2.
rewrite H3.
intuition.
Qed.

(* Hint Resolve perm_symm. *)


Lemma perm_conc_snoc: 
  forall (G' G:ctx) (A:Formula), Perm ((G;G'),A) ((G,A);G').
Proof.
intro G'.
induction G'.
simpl.
intuition.
intros.
simpl.
assert(Perm (((G; G'), f), A) (((G,f); G'), A)).
apply perm_snoc.
intuition.
assert(Perm (((G, f); G'), A) (((G, f), A); G') ).
intuition.
assert (Perm (((G, f), A); G') (((G, A), f); G') ).
intuition.
assert (Perm (((G, A), f); G') (((G, A); G'), f)).
apply perm_symm.
intuition.
eapply perm_trans.
exact H.
eapply perm_trans.
exact H0.
eapply perm_trans.
exact H1.
assumption.
Qed.

(* Hint Resolve perm_conc_snoc. *)

Lemma perm_conc_swap: forall (G' G:ctx), Perm (G;G') (G';G).
Proof.
intro.
induction G'; intros; simpl.
- rewrite ctx_empty_conc.
  apply perm_refl.
- assert(Perm ((G;G'),f) ((G';G),f)).
intuition.
assert(Perm ((G';G),f) ((G',f);G)).
apply perm_conc_snoc.
eapply perm_trans.
exact H.
assumption.
Qed.

(* Hint Resolve perm_conc_swap. *)


Lemma perm_cont: forall (G:ctx) (A:Formula), 
  Perm ((G,A);(G,A)) (((G;G),A),A).
Proof.
intro G.
simpl.
intros.
apply perm_snoc.
change ((G; G), A) with (G; (G,A)).
apply perm_conc_swap.
Qed.

(* Hint Resolve perm_cont. *)

