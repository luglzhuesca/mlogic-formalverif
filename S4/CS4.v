(** Natural Deduction System for necessity from
  ''A judmental reconstruction of Modal Logic'' 
   Pfenning & Davies (2001)
 *)

(** Original development by Selene Linares mater's thesis. *)

Require Import Coq.Program.Equality.
Require Import ModalLogic.
Require Import Context.

(** --------------- INFERENCE RULES --------------- *)

Inductive ND_Proof : ctx -> ctx -> Formula -> Prop :=

| nd_thyp : forall (D: ctx) (G G': ctx) (A: Formula),
             ND_Proof D (G,A ; G') A

| nd_vhyp : forall (D D': ctx) (G: ctx) (A: Formula),
             ND_Proof (D,A ; D') G A

| nd_intro : forall (D: ctx) (G: ctx) (A B: Formula),
             ND_Proof D (G,A) B -> ND_Proof D G (A ==> B)

| nd_apply : forall (D: ctx) (G: ctx) (A B: Formula),
             ND_Proof D G (A ==> B) -> 
             ND_Proof D G A-> ND_Proof D G B

| nd_boxI : forall (D: ctx) (G: ctx) (A: Formula),
             ND_Proof D empty A -> ND_Proof D G (Box A)

| nd_boxE : forall (D: ctx) (G: ctx) (A C: Formula),
             ND_Proof D G (Box A) -> ND_Proof (D,A) G C ->
             ND_Proof D G C.

Global Hint Constructors ND_Proof : NDS4.


(** --------------- STRUCTURAL RULES --------------- *)

Lemma nd_elem_thyps: 
  forall (D: ctx) (G : ctx) (A: Formula), elem A G -> ND_Proof D G A.
Proof.
intros.
assert( exists G1, exists G2, G=(G1,A);G2 ).
- apply elem_ctxsplit.
  assumption.
- destruct H0 as [G1'].
  destruct H0 as [G2'].
  rewrite H0.
  apply nd_thyp.
Qed.

Global Hint Resolve nd_elem_thyps : NDS4.


Lemma nd_elem_vhyps: 
  forall (D: ctx) (G : ctx) (A: Formula), 
         elem A D -> ND_Proof D G A.
Proof.
intros.
assert (exists D1, exists D2, D=(D1,A);D2).
- apply elem_ctxsplit.
  assumption.
- destruct H0 as [D1'].
  destruct H0 as [D2'].
  rewrite H0.
  apply nd_vhyp.
Qed.

Global Hint Resolve nd_elem_vhyps : NDS4.


Lemma nd_weakening_thyps : 
  forall (D: ctx) (G G': ctx) (A: Formula),
  ND_Proof D (G ; G') A -> forall (B : Formula), ND_Proof D (G, B ; G') A.
Proof.
intros.
dependent induction H ; eauto with NDS4.
- apply nd_elem_thyps.
  assert(elem A (G;G')).
  + rewrite <- x. 
    intuition.
  + apply elem_conc_split in H.
    destruct H; intuition.
- apply nd_intro.
  apply (IHND_Proof G (G',A) eq_refl B0).
Qed.

Global Hint Resolve nd_weakening_thyps : NDS4.

Lemma nd_weakening_vhyps : 
  forall (D D': ctx) (G : ctx) (A : Formula),
  ND_Proof (D ; D') G A -> forall (B : Formula), ND_Proof (D,B ; D') G A.
Proof.
intros.
dependent induction H; eauto with NDS4.
- apply nd_elem_vhyps.
  assert(elem A (D;D')).
  + rewrite <-x. intuition.
  + apply elem_conc_split in H.
    destruct H.
    * apply elem_conc_L.
      intuition.
    * apply elem_conc_R. 
    assumption.
- eapply nd_boxE.
  + apply IHND_Proof1.
    intuition.
  + rewrite ctx_snoc_conc.
    apply IHND_Proof2. 
    reflexivity.
Qed.

Global Hint Resolve nd_weakening_vhyps : NDS4.


Lemma nd_det: 
  forall (D G:ctx) (A B:Formula), ND_Proof D G (A ==> B) -> ND_Proof D (G,A) B.
Proof.
intros.
eapply nd_apply with A.
- apply (nd_weakening_thyps _ _ empty).
intuition.
- intuition.
Qed.

Global Hint Resolve nd_det : NDS4.


Lemma nd_exchange_thyps: 
  forall (D G:ctx) (A B C:Formula),
  ND_Proof D ((G,A),B) C -> ND_Proof D ((G,B),A) C.
Proof.
intros.
repeat (apply nd_intro in H).
apply (nd_apply _ _ B C).
- apply nd_det.
 apply (nd_weakening_thyps _ _ empty).
intuition.
- intuition.
Qed.

Global Hint Resolve nd_exchange_thyps : NDS4.


Lemma nd_weakening_thyps_ctxR: 
  forall (D: ctx) (G : ctx) (A: Formula), 
  ND_Proof D G A -> forall (G': ctx), ND_Proof D (G;G') A.
Proof.
intros.
induction G'; auto.
apply (nd_weakening_thyps _ (G;G') (empty)).
assumption.
Qed.

Global Hint Resolve nd_weakening_thyps_ctxR : NDS4.


Lemma nd_weakening_thyps_ctxL: 
  forall (G: ctx) (D : ctx) (A: Formula), 
  ND_Proof D G A -> forall (G': ctx), ND_Proof D (G';G) A.
Proof.
intro.
induction G; intros; simpl.
- rewrite <- (ctx_empty_conc G').
  apply nd_weakening_thyps_ctxR.
  assumption.
- apply nd_det.
  apply IHG.
  apply nd_intro.
  assumption.
Qed.

Global Hint Resolve nd_weakening_thyps_ctxL : NDS4.


Lemma nd_exch_thyps_conc: 
  forall (G' D G:ctx) (A B:Formula),
   ND_Proof D (G,A ;G') B -> ND_Proof D (G;G',A) B.
Proof.
intro.
induction G'; auto.
intros.
simpl in H.
apply nd_intro in H.
apply IHG' in H.
apply nd_det in H.
simpl.
apply nd_exchange_thyps.
assumption.
Qed.

Global Hint Resolve nd_exch_thyps_conc : NDS4.


Lemma nd_exch_thyps_snoc: 
 forall (G' D G:ctx) (A B:Formula),
 ND_Proof D (G;G',A) B -> ND_Proof D (G,A ;G') B.
Proof.
intro.
induction G'; auto.
intros.
simpl.
apply nd_det.
apply IHG'.
simpl.
apply nd_intro.
apply nd_exchange_thyps.
simpl in H.
assumption.
Qed.

Global Hint Resolve nd_exch_thyps_snoc : NDS4.


Lemma nd_intro_gen: 
  forall (G' D G:ctx) (A B:Formula),
   ND_Proof D (G,A ;G') B -> ND_Proof D (G;G') (A ==> B).
Proof.
intros.
apply nd_intro.
apply nd_exch_thyps_conc.
assumption.
Qed.

Global Hint Resolve nd_intro_gen : NDS4.


Theorem nd_subst:
  forall (D G: ctx) (A : Formula), (ND_Proof D G A) -> 
  forall (G':ctx) (B: Formula),  
    (ND_Proof D (G,A ;G') B) -> ND_Proof D (G;G') B.
Proof.
intros.
apply nd_intro_gen in H0.
eapply nd_apply.
exact H0.
apply nd_weakening_thyps_ctxR.
assumption.
Qed.

Global Hint Resolve nd_subst : NDS4.

(** 
  Transference relationship of formulas from one context to another *)

(** Valid formulas are necessary truths *)
Proposition val_to_true: 
  forall (D G:ctx) (A B:Formula),
   ND_Proof (D,A) G B -> ND_Proof D (G, #A) B.
Proof.
intros.
remember (nd_boxE D (G, # A) A B) as Helim.
apply Helim.
intuition.
 apply (nd_weakening_thyps _ _ empty).
simpl.
(* apply nd_weak_last. *)
exact H.
Qed.

Global Hint Resolve val_to_true : NDS4.


(* Corollary 3.11 *)
Corollary ctx_val_to_true:
  forall (D G: ctx) (A: Formula),
  ND_Proof D G A -> ND_Proof empty (boxed D; G) A.
Proof.
intro.
induction D; intros; simpl.
- rewrite ctx_empty_conc. exact H.
- apply val_to_true in H.
  apply IHD in H.
  apply nd_exch_thyps_snoc.
  assumption.
Qed.

Global Hint Resolve ctx_val_to_true : NDS4.


(* Corollary 3.14 Implication introduction for validity *)
Corollary nd_intro_val: forall (D G:ctx) (A B:Formula),
    ND_Proof (D,A) G B -> ND_Proof D G (#A ==> B).
Proof. 
auto with NDS4.
Qed.

Global Hint Resolve nd_intro_val : NDS4.

(* Proposition 3.15 Detachment for boxed formulas *)
Lemma nd_box_det: forall (D G:ctx) (A B:Formula),
    ND_Proof D G (#A ==> B) -> ND_Proof (D,A) G B.
Proof.
intros.
eapply nd_apply.
- apply (nd_weakening_vhyps _ empty _).
simpl.
(* apply nd_weak_vhyps_last. *)
exact H.
- 
apply nd_boxI.
rewrite <- (ctx_conc_empty (D,A)).
apply nd_vhyp.
Qed.

Global Hint Resolve nd_box_det : NDS4.



(* Proposition 3.16 Neccesary truths are valid  *)
Proposition true_to_val: forall (D G:ctx) (A B:Formula),
    ND_Proof D (G, #A) B -> ND_Proof (D,A) G B.
Proof.
intros.
apply nd_box_det.
apply nd_intro.
assumption.
Qed.

Global Hint Resolve true_to_val : NDS4.


(* Corollary 3.17  *)
Corollary ctx_true_to_val:
  forall (D G: ctx) (A: Formula),
  ND_Proof empty (boxed D; G) A -> ND_Proof D G A.
Proof.
intro.
induction D.
- simpl.
  intros.
  rewrite (ctx_empty_conc G) in H.
  assumption.
- intros.
  simpl in H.
  apply nd_intro_gen in H.
  apply IHD in H.
  apply nd_box_det in H.
  assumption.
Qed.

Global Hint Resolve ctx_true_to_val : NDS4.



(** Structural rules for contexts of valid formulas *)
Lemma nd_weakening_vhyps_ctxR: 
  forall (D: ctx) (G : ctx) (A: Formula), 
  ND_Proof D G A -> forall (D': ctx), ND_Proof (D;D') G A.
Proof.
intros.
induction D'; auto.
apply (nd_weakening_vhyps (D;D') (empty)).
assumption.
Qed.

Global Hint Resolve nd_weakening_vhyps_ctxR : NDS4.



(** more properties about vhyps 
they are easier to prove using the transference process *)

Lemma nd_exch_vhyps_conc: 
  forall (D' D G:ctx) (A B:Formula),
   ND_Proof ((D,A);D') G B -> ND_Proof (D;(D',A)) G B.
Proof.
intro.
induction D'; auto.
intros.
simpl in H.
apply nd_intro_val in H.
apply IHD' in H.
simpl in H.
apply val_to_true in H.
apply nd_box_det in H.
simpl.
apply true_to_val.
exact H.
Qed.

Global Hint Resolve nd_exch_vhyps_conc : NDS4.

Lemma nd_exch_vhyps_snoc: 
 forall (D' D G:ctx) (A B:Formula),
 ND_Proof (D;(D',A)) G B -> ND_Proof ((D,A);D') G B.
Proof.
intro.
induction D'; auto.
intros.
simpl.
apply nd_box_det.
apply IHD'.
simpl in H.
apply val_to_true in H.
apply nd_intro in H.
apply val_to_true in H.
apply nd_box_det in H.
intuition.
Qed.

Global Hint Resolve nd_exch_vhyps_snoc : NDS4.

Theorem nd_subst_vhyp:
  forall (D: ctx) (B: Formula), (ND_Proof D empty B) ->
  forall (D' G:ctx) (C: Formula),
    (ND_Proof ((D,B);D') G C) -> ND_Proof (D;D') G C.
Proof.
intros.
(* dependent induction H0; eauto.
case vhyp: analysis of (D0, B0); D'0 = (D, B); D'
case boxE: similar analysis...
*)
intros.
eapply (nd_boxI D G) in H.
eapply nd_weakening_vhyps_ctxR in H.
eapply nd_boxE in H.
exact H.
apply nd_box_det.
apply nd_exch_vhyps_conc in H0.
intuition.
Qed.

Global Hint Resolve nd_subst_vhyp : NDS4.


(** 
 Modal Axioms that characterize the logic S4 
 they are derivable in CS4
 *)

Theorem Axiom_T: 
  forall (D G: ctx) (A:Formula), ND_Proof D G ((#A) ==> A).
Proof.
intros.
apply nd_intro.
eapply nd_boxE; intuition.
Qed.

Global Hint Resolve Axiom_T : NDS4.


Theorem Axiom_4: 
  forall (D G: ctx) (A:Formula), ND_Proof D G (#A ==> ##A).
Proof.
intros.
apply nd_intro.
eapply nd_boxE. 
- apply (nd_elem_thyps _ _ (#A)). 
  intuition.
- intuition.
Qed.

Global Hint Resolve Axiom_4 : NDS4.


Theorem Axiom_K: 
  forall (D G: ctx) (A B:Formula),
  ND_Proof D G ((#(A ==> B)) ==> ((#A) ==> (#B))).
Proof.
intros.
repeat (apply nd_intro).
apply (nd_boxE D ((G, (# (A ==> B))), (# A)) A (#B)).
- intuition. 
- apply (nd_boxE _ _ (A ==> B) (#B)).
  * intuition.
  * apply nd_boxI.
    apply (nd_apply _ _ A B); intuition. 
Qed.

Global Hint Resolve Axiom_K : NDS4.



(** ----------------------------- *)
(* Examples *)

(** Attempt to prove the invalid theorem 
    empty | empty |- (A ==> #A) *)
Proposition false_theorem:
  forall (A: Formula), ND_Proof empty empty (A ==> #A).
Proof.
intro.
apply nd_intro.
(* Two options to prove ND_Proof empty (empty, A) (# A) 
   because of the form of the conclusion*)
(* 1. 
apply nd_boxI.
which demands to prove ND_Proof empty empty A *)
(* 2. 
eapply nd_boxE.
which demands to prove ND_Proof empty (empty, A) (# ?A)
and ND_Proof (empty, ?A) (empty, A) (# A)
two boxed formulas!!! *)
Abort.

(* Transitivity *)
Lemma trans_dett:
  forall (D G: ctx)(A B C : Formula),
  ND_Proof D G (A ==> B) -> ND_Proof D G (B ==> C) -> ND_Proof D G (A ==> C).
Proof.
intros.
apply nd_intro.
rewrite <- (ctx_conc_empty G) in H,H0.
eapply nd_weakening_thyps in H.
eapply nd_weakening_thyps in H0.
assert (ND_Proof D (G,A) A); intuition.
apply (nd_apply _ _ _ _ H) in H1.
apply (nd_apply _ _ _ _ H0) in H1.
assumption.
Qed.


(* Scott's rule and weakening *)
Proposition intro_boxk: 
  forall (D: ctx) (A: Formula), ND_Proof empty D A ->
  forall (G: ctx), ND_Proof D G (#A).
Proof.
intro.
induction D.
- intuition.
- intros.
  apply nd_intro in H.
  eapply IHD in H.
  apply nd_box_det.
  rewrite <- (ctx_conc_empty G).
  eapply (nd_subst _  _ (#(f==>A) ==> #f ==> #A)); intuition.
  eapply nd_apply.
  apply nd_thyp.
  exact H.
Qed.


Proposition intro_boxk4:
  forall (D: ctx) (A: Formula), ND_Proof D D A ->
  forall (G: ctx), ND_Proof D G (#A).
Proof.
intro.
induction D.
- intuition.
- intros.
  rewrite <- (ctx_conc_empty (D,f)).
  eapply (nd_subst_vhyp _ (#f)).
  intuition.
  apply nd_box_det.
  apply true_to_val.
  apply nd_intro_val.
  apply nd_det.
  rewrite <- (ctx_conc_empty G).
  eapply (nd_apply _ _ (#(f ==> A)) (#f ==> #A)).
  apply Axiom_K.
  apply nd_box_det.
  eapply (nd_apply _ _ (#(#f ==> f ==>A))); intuition.
Qed.


Proposition transfer_withoutbox:
  forall (G D: ctx) (A B: Formula), ND_Proof D (G, A) B -> ND_Proof (D,A) G B.
Proof.
intros.
apply nd_intro in H.
apply nd_box_det.
assert (Ht := Axiom_T D G A).
eapply trans_dett.
exact Ht.
exact H.
Qed.


Proposition ctx_transfer_withoutbox:
  forall (G G' D: ctx) (A: Formula), ND_Proof D (G;G') A -> ND_Proof (D;G) G' A.
Proof.
intro.
induction G.
- intros. 
  simpl.
  rewrite ctx_empty_conc in H.
  assumption.
- intros.
  apply nd_intro_gen in H.
  apply IHG in H.
  eapply nd_det in H.
  simpl.
  apply true_to_val.
  apply transfer_withoutbox in H.
  intuition.
Qed.

