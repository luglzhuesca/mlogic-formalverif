(** Equivalence of CS4 and HK4*)


Require Import Coq.Program.Equality.
Require Import ModalLogic.
Require Import Context.
Require Export CS4.
Require Export HK4.


(** From axiomatic HK4 to natural deduction CS4 *)

(* Lemma nd_thyp_last: 
  forall (D G:ctx) (A:Formula), ND_Proof D (G,A) A.
Proof.
auto.
Qed.

Hint Resolve nd_thyp_last.
 *)

Lemma AxI_CS: 
  forall (D G:ctx) (A:Formula), ND_Proof D G (A ==> A).
Proof.
intros.
apply nd_intro.
auto with NDS4.
Qed.

Global Hint Resolve AxI_CS : NDS4.


Lemma Ax1_CS: 
  forall (D G:ctx) (A B:Formula), ND_Proof D G (A ==> (B ==> A)).
Proof.
intros.
repeat apply nd_intro.
auto with NDS4.
Qed.

Global Hint Resolve Ax1_CS : NDS4.


Lemma Ax2_CS: 
  forall (D G:ctx) (A B:Formula), 
   ND_Proof D G ( (A ==> (A ==> B)) ==> (A ==> B)).
Proof.
intros.
repeat apply nd_intro.
eapply (nd_apply D _ A B).
- eapply (nd_apply D _ A (A ==> B)); auto with NDS4.
- apply nd_elem_thyps; intuition.
Qed.

Global Hint Resolve Ax2_CS : NDS4.


Lemma Ax3_CS: 
  forall (D G:ctx) (A B C:Formula), 
   ND_Proof D G ( (A ==> (B ==> C)) ==> (B ==> (A ==> C))).
Proof.
intros.
repeat apply nd_intro.
eapply nd_apply; auto with NDS4.
Qed.

Global Hint Resolve Ax3_CS : NDS4.

Lemma Ax4_CS: 
  forall (D G:ctx) (A B C:Formula), 
   ND_Proof D G ( (B ==> C) ==> ((A ==> B) ==> (A ==> C))).
Proof.
intros.
repeat apply nd_intro.
eapply nd_apply.
- apply nd_elem_thyps. 
  auto with contexts.
- eapply nd_apply; apply nd_elem_thyps; auto with contexts.
Qed.

Global Hint Resolve Ax4_CS : NDS4.


(* Theorem 5.1 From axiomatic to natural deduction proofs.*)
Theorem Hs4_to_Ns4:
  forall (G: ctx) (A: Formula), (G |- A) -> (ND_Proof empty G A).
Proof.
intros.
dependent induction H ; auto with NDS4.
eapply nd_apply.
- eapply nd_weakening_thyps_ctxR.
  exact IHDeriv2.
- apply nd_weakening_thyps_ctxL.
  exact IHDeriv1.
Qed.

(* Hint Resolve Hs4_to_Ns4. *)


(* Theorem 5.2 From natural deduction to axiomatic proofs *)
Theorem Ns4_to_Hs4:
  forall (D: ctx) (G: ctx) (A: Formula),
  (ND_Proof D G A) -> boxed D;G |- A.
Proof.
intros.
dependent induction H.
- apply Hyp.
  intuition.
- rewrite <- (ctx_empty_conc (boxed ((D, A); D'); G)).
  rewrite boxed_conc.
  simpl.
  eapply (MP _ empty (#A)).
  intuition.
  apply (AxBoxT _ A).
- simpl in IHND_Proof.
  apply DeductionTh.
  assumption.
- apply ctx_contraction.
  eapply MP.
  * exact IHND_Proof2.
  * assumption.
- apply GenNec.
  simpl in IHND_Proof.
  assumption.
- simpl in IHND_Proof2.
  apply deductionTh_genPremise in IHND_Proof2.
  apply ctx_contraction.
  eapply MP.
  * exact IHND_Proof1.
  * assumption.
Qed.

(* Hint Resolve Ns4_to_Hs4. *)

