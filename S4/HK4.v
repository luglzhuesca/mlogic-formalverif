(** Hilbert style System HK4 from
  ''Does the deduction theorem fail for modal logic?'' 
   Sara Negri & Raul Hakli (2012)
   *)

Require Import Coq.Program.Equality.
Require Import ModalLogic.
Require Import Omega.
Require Import Context.

(** --------------- INFERENCE RULES --------------- *)
(** System HK for modal logic with local hypotheses, 
  without negation but extended with modal axioms for T and 4.
  As remarked by the authors, there is no substitution rule,
  the axiom schemata is used to perform implicit substitutions 
  giving instances of axioms whenever it is needed 
*)


Inductive Deriv: ctx -> Formula -> Prop:=
| Hyp:  forall (G : ctx) (A: Formula), 
          elem A G -> Deriv G A 
      
| Ax1:  forall (G: ctx) (A B: Formula), 
        Deriv G (A ==> (B ==> A))
       
| Ax2:  forall (G: ctx) (A B: Formula), 
        Deriv G ((A ==> (A ==> B)) ==> (A ==> B))
       
| Ax3:  forall (G: ctx) (A B C: Formula), 
        Deriv G ((A ==> (B ==> C)) ==> (B ==>(A ==> C)))
       
| Ax4:  forall (G: ctx) (A B C: Formula), 
        Deriv G ((B ==> C) ==> ((A ==> B) ==> (A ==> C)))
       
| AxBoxK: forall (G: ctx) (A B: Formula), 
        Deriv G (Box(A ==> B) ==> ((Box A) ==> (Box B)))
        
| AxBoxT: forall (G : ctx) (A : Formula),
        Deriv G ((Box A) ==> A)
       
| AxBox4: forall (G : ctx) (A : Formula),
        Deriv G ((Box A) ==> (Box(Box A)))
       
| MP:   forall (G G': ctx) (A B: Formula),
        Deriv G A -> Deriv G' (A ==> B) -> Deriv (G';G) B
      
| Nec:  forall (G: ctx) (A: Formula), 
        Deriv empty A -> Deriv G (Box A).

Global Hint Constructors Deriv : HK4.

Notation "G |- A" := (Deriv G A) (at level 30).


(** ----------------------------- *)
(** 
 Verification of statements in the article and other useful lemmas, 
 some of them are the dettached versions of the axioms
 or generalizations of rules
 *)

Lemma AxI: 
  forall (G:ctx) (A:Formula), G |- (A ==> A).
Proof.
intros.
assert (H := Ax1 empty A A).
assert (H1:= Ax2 G A A).
rewrite <- (ctx_conc_empty G).
eapply MP in H1.
- exact H1.
- exact H.
Qed.

Global Hint Resolve AxI : HK4.


Lemma Ax3_dett: 
  forall (G:ctx) (A B C:Formula),
  G |- (A ==> B ==> C) -> G |- (B ==> A ==> C).
Proof.
intros.
rewrite <- (ctx_empty_conc G).
eapply MP.
- exact H.
- intuition.
Qed.

Global Hint Resolve Ax3_dett : HK4.


(** ----------------------------- *)
(* Tactic for solving axioms in DeductionTh *)
Ltac solve_axioms :=
match goal with 
  | |- (Deriv (conc empty ?G) (Impl ?A ?F)) =>
      eapply (MP _ _ F _); intuition
end.

(* Theorem 4.3 Deduction *)
Theorem DeductionTh: 
  forall (G: ctx) (A B: Formula), (G,A) |- B -> G |- (A ==> B).
Proof.
intros G A B H.
dependent induction H; rewrite <- (ctx_empty_conc G).
(* Hyp case *)
{ apply elem_inv in H.
  destruct H.
  + rewrite H.
    apply AxI.
  + eapply (MP _ _ A0 (A==> A0)); intuition. }
(* Axioms *)
1-7: solve_axioms.
(* MP case *)
{ rewrite ctx_empty_conc.
  assert (x':=x).
  apply ctx_decomposition in x.
  destruct x; destruct H1.
  + rewrite H1 in H.
    apply IHDeriv2 in H2.
    apply Ax3_dett in H2.
    rewrite <- (ctx_conc_empty G).
    apply (MP _ _ A0 (A ==>B)); intuition.
  + rewrite H1 in x'.
    simpl in x'.
    inversion x'.
    assert (empty |- ((A0 ==> B) ==> (A ==>A0) ==> (A ==> B))); intuition.
    apply (MP G' empty (A0 ==> B) ((A ==> A0) ==> A ==> B) H0) in H2.
    apply IHDeriv1 in H1.
    eapply MP.
    exact H1.
    rewrite (ctx_empty_conc G') in H2.
    exact H2. }
(* Nec case *)
{ eapply MP. 
  + apply Nec.
    exact H.
  + apply Ax1. }
Qed. 

Global Hint Resolve DeductionTh : HK4.


(* Corollary 4.1  Multiple Discharge*)
Corollary multihyp_discharge:
  forall (n:nat) (G: ctx) (A B: Formula),
  G;(replicate A n) |- B -> G |- (A==>B).
Proof.
intros.
dependent induction n; simpl in H ; rewrite <- (ctx_empty_conc G). 
- eapply (MP _ _ B (A ==> B)).
  + assumption.
  + apply Ax1.
- apply DeductionTh in H.
  apply IHn in H.
  eauto with HK4.
Qed.

Global Hint Resolve multihyp_discharge : HK4.


(* Corollary 4.2 Substitution or closure under composition *)
Corollary substitution:
  forall (G G': ctx) (A B: Formula),
  (G |- A) -> (G',A |- B) -> (G';G |- B).
Proof.
intros.
apply DeductionTh in H0.
eapply MP.
- exact H.
- exact H0.
Qed.

Global Hint Resolve substitution : HK4.


(* Theorem 4.2 Detachment or Inverse Deduction*)
Theorem detachment:
  forall (G: ctx) (A B: Formula), G |- (A ==> B) -> G,A |- B.
Proof.
intros.
assert ((empty,A)|- A); intuition.
change (G,A) with (G; (empty,A)).
eapply MP.
exact H0.
exact H.
Qed.

Global Hint Resolve detachment : HK4.


(* Theorem 4.3 General Deduction Theorem*)
Theorem deductionTh_genPremise: 
  forall (G' G: ctx) (A B: Formula), (G,A);G' |- B -> G;G' |- (A ==> B).
Proof.
intro.
induction G'.
- intros. simpl in H. simpl.  apply DeductionTh. intuition.
- intros.
simpl in H.
apply DeductionTh in H.
apply IHG' in H.
apply Ax3_dett in H.
apply detachment in H.
auto.
Qed.

Global Hint Resolve deductionTh_genPremise : HK4.


(** ----------------------------- *)
(* Lemma 4.1 Context Permutation *)
Lemma ctx_permutation: 
  forall (G G':ctx) (A:Formula), G;G' |- A -> G';G |- A.
Proof.
intro G.
induction G.
- intros.
  simpl.
  rewrite ctx_empty_conc in H.
  assumption.
- intros.
  simpl.
  apply detachment.
  apply IHG.
  apply deductionTh_genPremise in H.
  assumption.
Qed.

Global Hint Resolve ctx_permutation : HK4.


Lemma Ax2_dett: 
  forall (G:ctx) (A B:Formula), G |- (A ==> A ==> B) -> G |- (A ==> B).
Proof.
intros.
assert (H1 := Ax2 empty A B); intuition.
apply (MP _ _ (A ==> A ==> B) (A ==> B) H) in H1.
rewrite ctx_empty_conc in H1.
exact H1.
Qed.

Global Hint Resolve Ax2_dett : HK4.


(* Lemma 4.2 Context Contraction *)
Lemma ctx_contraction: 
  forall (G:ctx) (A:Formula), G;G |- A -> G |- A.
Proof.
intro.
induction G.
- auto.
- intros.
  apply deductionTh_genPremise in H.
  simpl in H.
  apply DeductionTh in H.
  apply Ax2_dett in H.
  apply detachment.
  apply IHG.
  assumption.
Qed.

Global Hint Resolve ctx_contraction : HK4.


Lemma transitivity:
  forall (G: ctx) (A B C: Formula),
  G |- ((A ==> B) ==> ((B ==> C) ==> (A==> C))).
Proof.
intros.
rewrite <- (ctx_empty_conc G).
eapply MP.
2: eapply Ax3.
intuition.
Qed.

Global Hint Resolve transitivity : HK4. 


Lemma trans_dett:
  forall (G G': ctx)(P Q R : Formula),
  G |- (P ==> Q) -> G' |- (Q ==> R) -> (G;G') |- (P ==> R).
Proof.
intros.
assert (K:= transitivity empty P Q R).
rewrite <- (ctx_empty_conc G).
eapply MP.
- exact H0.
- eapply MP.
  exact H.
  assumption.
Qed.

Global Hint Resolve trans_dett : HK4.


Lemma AxBoxK_dett: 
  forall (G:ctx) (A B:Formula), (G |- #(A ==> B)) -> G|- (#A ==> #B).
Proof.
intros.
rewrite <- (ctx_empty_conc G).
eapply MP.
- exact H.
- apply AxBoxK.
Qed.

Global Hint Resolve AxBoxK_dett : HK4.


(** Attempt to prove the invalid theorem 
    empty |- (A ==> #A) *)
Proposition false_theorem:
  forall (A: Formula), empty |- (A ==> #A).
Proof.
intro.
apply DeductionTh.
apply Nec.
(* here, it is needed to prove that empty |- A *)
Abort.


(** ----------------------------- *)
(** General necessitation *)
Lemma boxtrans: 
  forall (G:ctx) (A B:Formula), G |- (#(#A) ==> B) -> G |- (#A ==> B).
Proof.
intros.
assert (K:= AxBox4 empty A).
rewrite <- (ctx_empty_conc G).
eapply trans_dett.
- exact K.
- exact H.
Qed.

Global Hint Resolve boxtrans : HK4.


(* Lemma 4.3 General Neccesitation *)
Lemma GenNec: 
  forall (D:ctx) (A:Formula),
   boxed D |-A -> forall (G:ctx), boxed D; G |- # A.
Proof.
intro.
induction D; intros.
- intuition.
- apply ctx_permutation.
  simpl in H.
  apply DeductionTh in H.
  eapply IHD in H.
  apply AxBoxK_dett in H.
  apply boxtrans in H.
  simpl.
  apply detachment.
  apply ctx_permutation in H.
  exact H.
Qed.

Global Hint Resolve GenNec : HK4.


(** ----------------------------- *)
(* Examples *)
Proposition Scott:
  forall (G: ctx) (A: Formula), G |- A -> boxed G |- #A.
Proof.
intro.
induction G.
- intuition.
- intros.
  simpl.
  apply detachment.
  assert (Hk := AxBoxK empty f A).
  rewrite <- (ctx_empty_conc (boxed G)).
  eapply MP.
  2: exact Hk.
  apply IHG.
  intuition.
Qed.

Proposition T_rule:
  forall (G: ctx) (A: Formula), G |- A -> boxed G |- A.
Proof.
intro.
induction G.
- intuition.
- intros.
  simpl.
  apply detachment.
  apply IHG.
  apply DeductionTh in H.
  assert (Ht:= AxBoxT empty f).
  apply (trans_dett empty G (#f) f A) in H.
  rewrite ctx_empty_conc in H.
  assumption.
  assumption.
Qed.

Proposition Four_rule:
  forall (G: ctx) (A: Formula), (boxed G); G |- A -> boxed G |- #A.
Proof.
(* using dependent induction H; eauto.
   the pending cases are Hyp and MP where the induction hypotheses do not
   ease the proof. *)
intro.
induction G.
- intuition.
- intros.
  simpl in H.
  apply DeductionTh in H.
  apply deductionTh_genPremise in H.
  eapply IHG in H.
  apply AxBoxK_dett in H.
  assert (H4 := AxBox4 empty f).
  apply (trans_dett _ _ _ _ _ H4) in H.
  assert (Hk' := AxBoxK empty f A).
  apply (trans_dett _ _ _ _ _ H) in Hk'.
  apply Ax2_dett in Hk'.
  repeat (simpl in Hk').
  apply detachment in Hk'.
  rewrite ctx_empty_conc in Hk'.
  exact Hk'.
Qed.


Proposition Left_box:
  forall (G: ctx) (A B: Formula), G,A |- B -> G, (#A) |- B.
Proof.
intros.
assert (Ht := AxBoxT empty A).
assert (Hh := Hyp (G, #A) (#A) (elem_ctxhead (#A) G)).
apply (MP _ _ _ _ Hh) in Ht.
rewrite ctx_empty_conc in Ht.
apply (substitution _ _ _ B Ht) in H.
simpl in H.
apply DeductionTh in H.
apply ctx_contraction in H.
apply detachment.
exact H.
Qed.
