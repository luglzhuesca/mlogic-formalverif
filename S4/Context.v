(** Contexts and Properties *)

(** This module is for context manipulation.
Contexts are implemented as snoc lists where the focused element in the context 
is at the right-hand side.
 *)

Require Import Coq.Program.Equality.
Require Import ModalLogic.

Set Implicit Arguments.


(** Definition of Context of Formulae *)
Inductive ctx : Type :=
 | empty : ctx
 | snoc : ctx -> Formula -> ctx.

Global Hint Constructors ctx : contexts.

Notation " G , p " := (snoc G p) (at level 20, p at next level).


(** Definition of equality between contexts *)
Proposition eq_ctx_dec (G G': ctx): {G = G'} + {G <> G'}.
Proof.
intros.
repeat (decide equality).
Qed.

Global Hint Resolve eq_ctx_dec : contexts.

(** Decidability of empty context *)
Proposition eq_ctx_dec_empty (G:ctx): {G = empty} + {G <> empty}.
Proof.
intros.
repeat (decide equality).
Qed.

Global Hint Resolve eq_ctx_dec_empty : contexts.


(** --------------- CONTEXT OPERATIONS --------------- *)

(** Context concatenation *)
Fixpoint conc (G G': ctx) : ctx :=
  match G' with
  | empty => G
  | snoc D q => snoc (conc G D) q
  end.

Global Hint Unfold conc : contexts.

Notation " G ; D " := (conc G D) (at level 20).

(** Membership function *)
Fixpoint elem (a: Formula) (G: ctx) : Prop :=
  match G with
  | empty => False
  | G',b => b = a \/ elem a G'
  end.

Global Hint Unfold elem : contexts.


(** Definition of a boxed context *)
Fixpoint boxed (c:ctx) : ctx :=
  match c with
  | empty => empty
  | snoc G' b  => snoc (boxed G') (Box b)
  end.
  
Global Hint Unfold boxed : contexts.


(** Function that constructs a context with 
n occurrences of a given formula*)

Fixpoint replicate (A: Formula) (n: nat): ctx :=
  match n with
  | 0 => empty
  | S n => (replicate A n,A)
  end.

Global Hint Unfold replicate : contexts.


(** --------------- CONTEXT PROPERTIES ---------------*)

(** ABOUT ELEM *)


Lemma elem_ctxhead:
  forall (A: Formula) (G: ctx), elem A (G,A).
Proof.
intros.
simpl.
intuition.
Qed.

Global Hint Resolve elem_ctxhead : contexts.


Lemma elem_inv:
  forall (A B:Formula) (G:ctx), elem B (G,A) -> (A = B) \/ elem B G.
Proof.
intros.
inversion H.
- left. assumption.
- right. assumption.
Qed.

Global Hint Resolve elem_inv : contexts.

Lemma elem_ctxsplit:
  forall (A: Formula) (G: ctx), elem A G -> exists G1, exists G2, G=(G1,A);G2.
Proof.
intros.
induction G.
- elim H.
- inversion H.
  + exists G. exists empty.
    rewrite H0. 
    reflexivity.
  + apply IHG in H0.
    destruct H0. destruct H0.
    rewrite H0 in *.
    exists x. exists (x0,f).
    reflexivity.
Qed.

Global Hint Resolve elem_ctxsplit : contexts.


Lemma elem_conc_split: 
  forall (A:Formula) (G G':ctx), elem A (G;G') -> elem A G \/ elem A G'.
Proof.
intros.
induction G'; simpl in H.
- left; intuition.
- destruct H.
  + right. intuition.
  + apply IHG' in H.
    destruct H; intuition.
Qed.

Global Hint Resolve elem_conc_split : contexts. 

Lemma elem_conc_L: 
  forall (A:Formula) (G:ctx), elem A G -> forall (G':ctx), elem A (G;G').
Proof.
intros.
induction G'.
- simpl. trivial.
- case (elem_conc_split A G G' IHG'); intros; simpl; right; assumption.
Qed.

Global Hint Resolve elem_conc_L : contexts.


Lemma elem_conc_R: 
  forall (A:Formula) (G' G:ctx), elem A G' -> elem A (G;G').
Proof.
intros A G'.
induction G'; intros.
- inversion H.
- simpl conc.
  inversion H; simpl.
  + left. exact H0.
  + right. apply IHG'. exact H0.
Qed.

Global Hint Resolve elem_conc_R : contexts.


(** --------------- ABOUT SNOC and CONC --------------- *)

Proposition ctx_eq_snoc:
  forall (G G':ctx) (A:Formula), G = G' -> G,A = G',A.
Proof.
intros.
rewrite H.
reflexivity.
Qed.

Global Hint Resolve ctx_eq_snoc : contexts.

(* 
Lemma ctx_eq_conc_empty:
  forall (G G': ctx), (G;G') = empty -> G = empty /\ G'= empty.
Proof.
intros.
destruct G; destruct G'; (split; reflexivity) || discriminate H.
Qed.

Hint Resolve ctx_eq_conc_empty.
 *)

Lemma ctx_empty_conc: 
  forall (G : ctx) , (empty;G) = G.
Proof.
intros.
induction G; simpl; intuition.
Qed.

Global Hint Resolve ctx_empty_conc : contexts.

Lemma ctx_conc_empty: 
  forall (G : ctx) , (G;empty) = G.
Proof.
intuition.
Qed.

Global Hint Resolve ctx_conc_empty : contexts.


Lemma ctx_snoc_conc:
  forall (G G': ctx) (A B: Formula), (((G,A);G'),B) = ((G,A);(G',B)).
Proof.
intros.
simpl.
reflexivity.
Qed.

Global Hint Resolve ctx_snoc_conc : contexts.

Lemma ctx_decomposition:
  forall (G G' D: ctx) (A: Formula), 
  (G;G' = D,A) -> (G' = empty /\ G = D,A) \/ exists (G'':ctx), G'=(G'',A).
Proof.
intros.
case_eq G'; intro.
- left. intuition. rewrite H0 in H. simpl in H. assumption.
- intros .
  right. rewrite H0 in H. simpl in H.
  assert (f=A).
  + inversion H. reflexivity.
  + rewrite H1.
    exists c. reflexivity.
Qed.

Global Hint Resolve ctx_decomposition : contexts.



Lemma boxed_conc:
forall (G G':ctx), boxed (G;G') = (boxed G);(boxed G').
Proof.
intros.
induction G'.
- reflexivity.
- simpl.
  rewrite IHG'.
  reflexivity.
Qed.

Global Hint Resolve boxed_conc : contexts.
