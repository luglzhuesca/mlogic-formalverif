
Require Import Coq.Program.Equality.
Require Import ModalLogic.
Require Import Context.
Require Export HS4.
Require Export JNDS4.
Require Export NDS4.
Require Export LJS4.
Require Export GS4.

(* ----------------------------------------------*)
(** Equivalence between H and JND *)
(* From Hilbert H to natural deduction JND *)

Theorem H_to_JND:
  forall (G: ctx) (A: Formula), (G |- A) -> (JND_Proof empty G (JTrue A)).
Proof.
intros.
dependent induction H; auto with CS4 context; intuition.
- apply jnd_intro. eapply jnd_andE_l; intuition.
- apply jnd_intro. eapply jnd_andE_r; intuition.
- repeat (apply jnd_intro). intuition.
  eapply jnd_orE.
  apply jnd_thyp_last.
  intuition.
  intuition.
- eapply jnd_apply.
  apply (jnd_weakening_thyps_ctxR _ _ _ IHDeriv2); intuition.
  apply (jnd_weakening_thyps_ctxL _ _ _ IHDeriv1).
Qed.

Global Hint Resolve H_to_JND : EQ.


Corollary H_to_JND_poss:
  forall (G: ctx) (A: Formula), 
  (G |- $A) -> (JND_Proof empty G (JPoss A)).
Proof.
intros.
apply H_to_JND in H.
eapply jnd_diaE in H.
exact H.
apply jnd_tp.
rewrite <- (ctx_conc_empty (empty,A)).
apply jnd_thyp.
Qed.

Global Hint Resolve H_to_JND_poss : EQ.


(* From judmental natural deduction to axiomatic *)

Theorem JND_to_H:
  forall (D: ctx) (G: ctx) (A: Judgm),
  (JND_Proof D G A) -> boxed D;G |- (transl A).
Proof.
intros.
dependent induction H; simpl in *; auto with HK4 context.
- rewrite boxed_conc.
  rewrite <- (ctx_empty_conc ((boxed (D, B); boxed D'); G)). 
  eapply (MP _ empty (#B) B).
  + simpl; intuition.
  + apply (AxBoxT _ B).
- apply ctx_contraction.
  eapply MP.
  + apply IHJND_Proof2; intuition.
  + eapply IHJND_Proof1; intuition.
- apply ctx_contraction.
  eapply MP. exact IHJND_Proof2.
  apply ctx_contraction.
  eapply MP. exact IHJND_Proof1.
  apply Ax5.
- apply ctx_contraction.
  eapply MP.
  exact IHJND_Proof.
  apply Ax6.
- apply ctx_contraction.
  eapply MP.
  exact IHJND_Proof.
  apply Ax7.
- apply ctx_contraction.
  eapply MP.
  exact IHJND_Proof.
  apply Ax8.
- apply ctx_contraction.
  eapply MP.
  exact IHJND_Proof.
  apply Ax9.
- apply ctx_contraction.
  apply DeductionTh in IHJND_Proof2.
  apply DeductionTh in IHJND_Proof3.
  eapply MP.
  exact IHJND_Proof1.
  apply ctx_contraction.
  eapply MP.
  apply IHJND_Proof3.
  apply ctx_contraction.
  eapply MP.
  apply IHJND_Proof2.
  apply Ax10.
- eapply deductionTh_genPremise in IHJND_Proof2; intuition.
  apply ctx_contraction.
  eapply MP.
  + apply IHJND_Proof1; intuition.
  + apply IHJND_Proof2.
- apply ctx_contraction.
  eapply MP.
  + exact IHJND_Proof1.
  + simpl. simpl in IHJND_Proof2.
    apply deductionTh_genPremise in IHJND_Proof2.
    exact IHJND_Proof2.
- apply DeductionTh in IHJND_Proof2.
  eapply GenNec in IHJND_Proof2.
  apply AxDiaK_dett in IHJND_Proof2.
  apply (MP _ _ _ _ IHJND_Proof1) in IHJND_Proof2.
  apply ctx_contraction in IHJND_Proof2.
  assert (boxed D; G |- (($ $ C) ==> $ C)); intuition.
  apply (MP _ _ _ _ IHJND_Proof2) in H1.
  apply ctx_contraction in H1.
  exact H1.
Qed.

Global Hint Resolve JND_to_H : EQ.


(* ----------------------------------------------*)
(** Equivalence between LJ and the axiomatic sytem  *)
(** From Hilbert H to sequent calculus LJ *)
Theorem HK_to_LJ:
  forall (G: ctx) (A: Formula), (G |- A) -> (LJ_S4 G A).
Proof.
intros.
dependent induction H; rewrite <- (ctx_conc_empty G); auto with LJ context.
- repeat (apply lj_impR).
change ((((G, (A ==> B ==> C)), B), A); empty) with ((G, (A ==> B ==> C));((empty, B), A)).
apply lj_impL; intuition.
- repeat (apply lj_impR).
change ((((G, (B ==> C)), (A ==> B)), A); empty) with ((G, (B ==> C));(empty, (A ==> B), A)).
apply lj_impL; intuition.
- rewrite <- (ctx_conc_empty G).
repeat (apply lj_impR). 
apply lj_disjL; intuition.
- apply lj_perm_ctx.
 rewrite <- (ctx_conc_empty G). 
apply (lj_cut _ _ A); intuition.
- rewrite <- (ctx_empty_conc G).
change empty with (boxed empty).
apply lj_weak_ctx.
intuition.
Qed.


Theorem LJ_to_HK:
  forall (D: ctx) (G: ctx) (A: Formula),
  (LJ_S4 G A) -> G |- A.
Proof.
intros.
dependent induction H; auto with HK4 context.
- apply DeductionTh in IHLJ_S4_2.
apply (MP _ _ A B IHLJ_S4_1) in IHLJ_S4_2.
auto with context HK4.

- apply ctx_contraction.
apply (MP _ _ B).
intuition.
apply ctx_contraction.
apply (MP _ _ A).
intuition.
apply deductionTh_genPremise in IHLJ_S4.
apply deductionTh_genPremise in IHLJ_S4.
auto with HK4 context.

- assert (empty |- (A==> (B==>(A & B)))); auto with HK4.
rewrite <- (ctx_empty_conc G).
rewrite <- (ctx_conc_empty (empty;G)).
apply ctx_contraction_gen.
simpl. rewrite ctx_conc_conc.
apply (MP _ _ B). exact IHLJ_S4_2.

apply (MP _ _ A). exact IHLJ_S4_1.
assumption.

- assert (empty |- ((A ==> C) ==> ((B==>C) ==>(A \ B) ==> C))); auto with HK4.
apply inverseDT_genPremise.
rewrite <- (ctx_empty_conc (G;G')).
apply deductionTh_genPremise in IHLJ_S4_1.
apply deductionTh_genPremise in IHLJ_S4_2.
apply (MP _ _ _ _ IHLJ_S4_1) in H1.
apply (MP _ _ _ _ IHLJ_S4_2) in H1.
rewrite <- (ctx_conc_empty (empty;(G;G'))).
apply (ctx_contraction_gen (G;G')).
simpl. rewrite ctx_conc_conc. assumption.

- rewrite <- (ctx_empty_conc G). 
eapply MP.
exact IHLJ_S4.
auto with HK4.

- rewrite <- (ctx_empty_conc G). 
eapply MP.
exact IHLJ_S4.
auto with HK4.

- apply inverseDT_genPremise.
 apply ctx_contraction.
 apply deductionTh_genPremise.
apply (substitution _ _ (B==>C)); intuition.
change (((G; G'), (A ==> B)), (B ==> C)) with ((G; G';(empty, (A ==> B))); (empty, (B ==> C))).
apply ctx_contraction_gen.
rewrite (ctx_conc_conc).
apply ctx_permutation_gen.
simpl.
change ((((empty, (A ==> B)); (G; G')), (A ==> B)), (B ==> C)) with (((empty, (A ==> B)); (G; G')); ((empty, (A ==> B)), (B ==> C))).

apply ctx_permutation.

apply (substitution _ _ A); intuition.

- assert (G' |- (#A ==>A)); intuition.
assert ((empty, (# A)) |- #A); intuition.
apply (MP _ _ (#A) A H1) in H0.
assert (K := weakening _ B IHLJ_S4 (empty,#A)); intuition.
apply ctx_contraction.
apply (substitution _ _ A); auto with HK4 context.

- 

apply weakening.
change (boxed G, ($ A)) with (boxed G;(empty,($ A))).
apply (elim_dia _ _ A); intuition.
Qed.


(* ----------------------------------------------*)
(* Equivalence between dual-context ND without annotations 
and dual-context Sequent Calculus *)

(* Theorem 
   From Dual Contex Natural Deduction System to Dual Context Sequent Calculus *)
Theorem NDS4_to_DCSC:
  forall (D: ctx) (G: ctx) (A: Formula),
  (ND_Proof D G A) -> (DC_Proof D G A).
Proof.
intros.
dependent induction H; eauto with GS4 NDS4 context.
- apply dc_intro_inv in IHND_Proof1.
  apply dc_ctx_contraction.
  apply dc_ctx_contractionV.
  eapply dc_cut. 
  exact IHND_Proof2.
  assumption.
- rewrite <- (ctx_conc_empty G). 
  rewrite <- (ctx_conc_empty D). 
  apply (dc_cut D empty G empty (A & B) A).
  assumption.
  rewrite <- (ctx_conc_empty (empty, (A&B))).
  apply dc_conjL.
  apply GS4.dc_elem_thyps.
  intuition.
- rewrite <- (ctx_conc_empty G).
  rewrite <- (ctx_conc_empty D). 
  apply (dc_cut D empty G empty (A & B) B).
  assumption.
  rewrite <- (ctx_conc_empty (empty, (A&B))).
  apply dc_conjL.
  apply GS4.dc_elem_thyps.
  intuition.
- apply dc_ctx_contraction.
  apply dc_ctx_contractionV. 
  eapply dc_cut.
  apply IHND_Proof1.
  intuition. 
- apply dc_ctx_contraction.
  apply dc_ctx_contractionV.
  eapply dc_cut.
  apply IHND_Proof1.
  intuition. 
- apply dc_ctx_contraction.
  apply dc_ctx_contractionV.
  eapply dc_cut.
  apply IHND_Proof1.
  intuition. 
Qed.

Global Hint Resolve NDS4_to_DCSC: EQ. 
   
(* Theorem 
   From Dual Context Sequent Calculus to Dual Contex Natural Deduction System *)
Theorem DCSC_to_NDS4:
  forall (D: ctx) (G: ctx) (A: Formula),
  (D|G |-s A) -> (D|G |-4 A).
Proof.
intros.
dependent induction H. (* eauto with NDS4 GS4 contexts.*)
- apply nd_thyp.
- apply nd_vhyp.
- apply nd_intro.
  assumption.
- apply nd_AndI.
  assumption.
  assumption.
- apply nd_OrIl.
  assumption.
- apply nd_OrIr.
  assumption.
- apply nd_BoxI.
  assumption.
- apply nd_DiaI.
  assumption.
- apply (nd_OrE D ((G, (A \ B)); G') A B C).
  + apply nd_thyp.
  + rewrite ctx_snoc_conc.
    apply nd_weakening_thyps.
    apply nd_exch_thyps_conc.
    assumption.
  + rewrite ctx_snoc_conc.
    apply nd_weakening_thyps.
    apply nd_exch_thyps_conc.
    assumption.
- apply (nd_subst D (G,(A&B)) A).
  + apply (nd_AndEl D (G, (A&B)) A B).
    rewrite <- (ctx_conc_empty (G, (A&B))).
    apply nd_thyp.
  + apply (nd_subst D ((G, (A & B)), A) B).
    * apply (nd_AndEr D ((G, (A&B)),A) A B).
      rewrite <- (ctx_conc_empty ((G, (A & B)), A)).
      apply nd_exch_thyps_snoc.
      rewrite ctx_snoc_conc.
      apply nd_thyp.
    * apply nd_exch_thyps_snoc.
      rewrite ctx_snoc_conc.
      apply nd_exch_thyps_snoc.
      rewrite ctx_snoc_conc.
      apply nd_weakening_thyps.
      apply nd_exch_thyps_conc.
      apply nd_exch_thyps_conc.
      assumption.
- rewrite <- (ctx_conc_empty ((G, (A ==> B)); G')).
  apply (nd_subst D ((G, (A ==> B)); G') A).
  + assumption.
  + apply (nd_subst D (((G, (A ==> B)); G'), A) B).
    *  apply (nd_ImpE D (((G, (A ==> B)); G'), A) A B).
       rewrite ctx_snoc_conc.
       apply nd_thyp.
       rewrite <- (ctx_conc_empty (((G, (A ==> B)); G'), A)).
       apply nd_thyp.
    * rewrite ctx_conc_empty.
      apply nd_exchange_thyps.
      rewrite <- (ctx_conc_empty ((((G, (A ==> B)); G'), B), A) ).
      apply nd_weakening_thyps.
      rewrite ctx_conc_empty.
      rewrite <- ctx_conc_snoc.
      apply nd_weakening_thyps.
      apply nd_exch_thyps_conc.
      assumption.
-  eauto with NDS4 GS4 context.
-  eauto with NDS4 GS4 context.
-  eauto with NDS4 GS4 context.
(*apply (nd_subst ((D, (A \ B)); D') G (A \ B)).  esta es con la que no puede *) 
 (*  eapply nd_subst_vhyp.
  apply nd_vhyp.
  apply nd_weakening_vhyps.
  apply (nd_OrE (D;D') ((G, (A \ B)); G') A B C).
  + apply nd_thyp.
  + rewrite <- ctx_conc_snoc.
    apply nd_weakening_thyps.
    apply nd_exch_thyps_conc.
    assumption.
  + rewrite <- ctx_conc_snoc.
    apply nd_weakening_thyps.
    apply nd_exch_thyps_conc.
    assumption. *)
- apply (nd_subst_vhyp (D, (A & B)) A).
  + rewrite <- (ctx_conc_empty empty).
    rewrite <- (ctx_conc_empty (D, (A & B))).
    apply (nd_subst ((D, (A & B));empty) empty (A&B)).
    apply nd_vhyp.
    apply (nd_AndEl ((D, (A & B)); empty) ((empty, (A & B)); empty) A B).
    apply nd_thyp.
  + apply (nd_subst_vhyp (((D, (A & B)), A)) B).
    * rewrite <- (ctx_conc_empty empty).
      apply (nd_subst (((D, (A & B)), A)) empty (A & B)).
      apply nd_elem_vhyps.
      intuition.
      apply (nd_AndEr ((D, (A & B)), A) ((empty, (A & B)); empty) A B).
      apply nd_thyp.
    * apply nd_exch_vhyps_snoc.
      apply nd_exch_vhyps_snoc.
      apply nd_weakening_vhyps.
      apply nd_exch_vhyps_conc.
      apply nd_exch_vhyps_conc.
      assumption.
- rewrite <-(ctx_conc_empty G).
  apply (nd_subst ((D, (A ==> B)); D') G B).
  apply (nd_ImpE ((D, (A ==> B)); D') G A B).
  apply (nd_vhyp).
  assumption.
  exact IHDC_Proof2.
- apply (nd_boxE ((D, (# A)); D') G A B).
  apply nd_vhyp.
  rewrite ctx_snoc_conc.
  apply nd_weakening_vhyps.
  apply nd_exch_vhyps_conc.
  assumption.
- eauto with NDS4 GS4 context.
- rewrite <- (ctx_conc_empty D).
eapply nd_subst_vhyp.
exact IHDC_Proof1.
assumption.
Qed.

Global Hint Resolve DCSC_to_NDS4: EQ. 

(* -------------------------------------------------*)
(* Equivalence between ND without annotations and axiomatic system *)
Theorem NDS4_to_H:
  forall (D: ctx) (G: ctx) (A: Formula),
  (D|G |-4 A) -> Deriv (boxed D;G) A.
Proof.
intros.
dependent induction H; auto with HK4 context;   simpl in *.
- apply weakening. rewrite boxed_conc. simpl.
  apply inverseDT_genPremise.
  intuition.
- apply ctx_contraction.
  eapply MP.
  exact IHND_Proof2.
  assumption.
- apply ctx_contraction.
  eapply MP.
  exact IHND_Proof2.
  apply ctx_contraction.
  eapply MP.
  exact IHND_Proof1.
  apply Ax5.
- apply ctx_contraction.
  eapply MP.
  exact IHND_Proof.
  apply Ax6.
- apply ctx_contraction.
  eapply MP.
  exact IHND_Proof.
  apply Ax7.
- apply ctx_contraction.
  eapply MP.
  exact IHND_Proof.
  apply Ax8.
- apply ctx_contraction.
  eapply MP.
  exact IHND_Proof.
  apply Ax9.
- apply ctx_contraction.
  apply DeductionTh in IHND_Proof2.
  apply DeductionTh in IHND_Proof3.
  eapply MP.
  exact IHND_Proof1.
  apply ctx_contraction.
  eapply MP.
  apply IHND_Proof3.
  apply ctx_contraction.
  eapply MP.
  apply IHND_Proof2.
  apply Ax10.
- eapply deductionTh_genPremise in IHND_Proof2; intuition.
  apply ctx_contraction.
  eapply MP.
  + apply IHND_Proof1; intuition.
  + apply IHND_Proof2.
- eapply elim_dia.
  + exact IHND_Proof1.
  + exact IHND_Proof2.
Qed.

Global Hint Resolve NDS4_to_H : EQ.



Theorem H_to_NDS4:
  forall (G: ctx) (A: Formula),
  (G |- A) -> (empty|G |-4 A).
Proof.
intros.
dependent induction H; simpl; auto with NDS4 context.
- repeat (apply nd_intro).
apply (nd_ImpE _ _ A); intuition.
- repeat (apply nd_intro).
apply (nd_ImpE _ _ B); intuition.
-  repeat (apply nd_intro).
apply (nd_ImpE _ _ B); intuition.
- apply nd_intro. eapply nd_AndEl; intuition.
- apply nd_intro. eapply nd_AndEr; intuition.
- repeat (apply nd_intro). intuition.
- repeat (apply nd_intro). 
  apply (nd_OrE _ _ A B); simpl; intuition.
  apply (nd_ImpE _ _ A); intuition. 
  apply nd_elem_thyps; simpl; intuition.
- eapply nd_ImpE. 
  apply nd_weakening_thyps_ctxR. exact IHDeriv2.
  intuition.
Qed.

Global Hint Resolve H_to_NDS4 : EQ.


(* -------------------------------------------------*)
(* Equivalence between JND and ND *)
Corollary JND_to_ND: 
  forall (D: ctx) (G: ctx) (J: Judgm),
  (JND_Proof D G J) -> (ND_Proof D G (transl(J))).
Proof.
intros.
destruct J; simpl in *.
- apply JND_to_H in H.
  apply H_to_NDS4 in H.
  intuition.
- apply JND_to_H in H.
  apply H_to_NDS4 in H.
  intuition.
Qed.

Global Hint Resolve JND_to_ND : EQ.

Corollary ND_to_JND: 
  forall (D: ctx) (G: ctx) (A: Formula),
  (ND_Proof D G A) -> (JND_Proof D G (JTrue A)).
Proof.
intros.
apply NDS4_to_H in H.
apply H_to_JND in H.
intuition.
Qed.

Global Hint Resolve ND_to_JND : EQ.

