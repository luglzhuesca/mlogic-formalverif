(** GS4 from 'On Interactive Proof-Search for Constructive Modal Necessity'
   Dual-context sequent calculus for the
full constructive modal logic S4. 
 *)

Require Import Coq.Program.Equality.
Require Import ModalLogic.
Require Import Context.


(** --------------- INFERENCE RULES --------------- *)
(** The distinction between valid and true hypotheses is expressed by 
    separate sets of hypotheses, ie two contexts as arguments for the
    hypothetical judments.
    This definition incorporates the possibility judgment in 
    dedicated rules with explicit constructors. *)
Inductive DC_Proof : ctx -> ctx -> Formula -> Prop :=

| dc_thyp : forall (D: ctx) (G G': ctx) (A: Formula),
             DC_Proof D ((G,A);G') A

| dc_vhyp : forall (D D': ctx) (G: ctx) (A: Formula),
             DC_Proof ((D,A);D') G A

| dc_impR : forall (D: ctx) (G: ctx) (A B: Formula),
             DC_Proof D (G,A) B -> DC_Proof D G (A ==> B)

| dc_conjR : forall (D: ctx) (G: ctx) (A B: Formula),
             DC_Proof D G A ->
             DC_Proof D G B -> DC_Proof D G (A & B)

| dc_disjR_L : forall (D: ctx) (G: ctx) (A B: Formula),
             DC_Proof D G A -> DC_Proof D G (A \ B)

| dc_disjR_R : forall (D: ctx) (G: ctx) (A B: Formula),
             DC_Proof D G B -> DC_Proof D G (A \ B)

| dc_boxR : forall (D: ctx) (G: ctx) (A: Formula),
             DC_Proof D empty A -> DC_Proof D G (# A)

| dc_diaR : forall (D: ctx) (G: ctx) (A: Formula),
            DC_Proof D G A -> DC_Proof D G ($ A)

| dc_disjL : forall (D: ctx) (G G': ctx) (A B C: Formula),
             DC_Proof D ((G,A) ; G') C ->
             DC_Proof D ((G,B) ; G') C -> DC_Proof D ((G, (A \ B));G') C

| dc_conjL : forall (D: ctx) (G G': ctx) (A B C : Formula),
             DC_Proof D ((G,A,B);G') C -> DC_Proof D ((G,(A & B));G') C

| dc_impL : forall (D: ctx) (G G': ctx) (A B C: Formula),
            DC_Proof D ((G,(A==>B));G') A -> DC_Proof D ((G,B);G') C ->
            DC_Proof D ((G,(A==>B));G') C 

| dc_boxL : forall (D: ctx) (G G': ctx) (A B: Formula),
            DC_Proof (D,A) (G;G') B -> DC_Proof D ((G,#A);G') B

| dc_diaL: forall (D:ctx) (G G': ctx) (A C:Formula),
            DC_Proof D (empty,A) ($ C) -> DC_Proof D ((G,$A);G') ($ C)

| dc_disjLV : forall (D D':ctx) (G:ctx) (A B C: Formula),
            DC_Proof ((D, (A \ B));D') (G,A) C -> 
            DC_Proof ((D, (A \ B));D') (G,B) C -> 
            DC_Proof ((D, (A \ B));D') G C

| dc_conjLV : forall (D D':ctx) (G: ctx) (A B C: Formula),
            DC_Proof ((D,A,B);D') G C -> DC_Proof ((D,(A & B));D') G C

| dc_impLV : forall (D D':ctx) (G: ctx) (A B C: Formula),
             DC_Proof ((D,(A==>B));D') G A -> 
             DC_Proof ((D, (A ==> B));D') (G,B) C ->
             DC_Proof ((D,(A==>B));D') G C

| dc_boxLV : forall (D D':ctx) (G: ctx) (A B : Formula),
            DC_Proof ((D,A);D') G B -> DC_Proof ((D,#A);D') G B

| dc_diaLV: forall (D D' : ctx) (G : ctx) (A C : Formula),
            DC_Proof ((D,$A);D') (empty, A) ($C) -> DC_Proof ((D,$A);D') G ($C)
(* enhanced cut *)
| dc_cutV : forall (D:ctx) (G: ctx) (A B : Formula),
            DC_Proof D empty A -> DC_Proof (D,A) G B -> DC_Proof D G B.

Global Hint Constructors DC_Proof : GS4.

Notation "D | G |-s A" := (DC_Proof D G A) (at level 30).

Lemma dc_elem_thyps: 
  forall (D: ctx) (G : ctx) (A: Formula), elem A G -> D | G |-s A.
Proof.
intros.
assert( exists G1, exists G2, G=(G1,A);G2 ).
- apply elem_ctxsplit.
  assumption.
- destruct H0 as [G1'].
  destruct H0 as [G2'].
  rewrite H0.
  apply dc_thyp.
Qed.

Global Hint Resolve dc_elem_thyps : GS4. 


Lemma dc_elem_vhyps: 
  forall (D: ctx) (G : ctx) (A: Formula), 
         elem A D -> D|G|-s A.
Proof.
intros.
assert (exists D1, exists D2, D=(D1,A);D2).
- apply elem_ctxsplit.
  assumption.
- destruct H0 as [D1'].
  destruct H0 as [D2'].
  rewrite H0.
  apply dc_vhyp.
Qed.

Global Hint Resolve dc_elem_vhyps : GS4. 


(** Example 6.1 *)
Proposition deriv_example:
  forall (A B: Formula), empty|empty |-s ((#(A ==> B)) ==> $(#A ==> $B)).
Proof.
intros.
apply dc_impR.
apply dc_diaR.
rewrite <- (ctx_conc_empty (empty, (# (A ==> B)))).
apply dc_impR.
apply dc_diaR.
rewrite <- (ctx_conc_snoc).
apply dc_boxL.
simpl.
rewrite <- (ctx_conc_empty (empty, (# A))).
apply dc_boxL; simpl; intuition.
rewrite <- (ctx_conc_empty (empty, (A ==> B))).
rewrite <- (ctx_conc_snoc).
apply dc_impLV; simpl.
- apply dc_elem_vhyps. intuition.
- apply dc_elem_thyps. intuition.
Qed.



(** --------------- STRUCTURAL RULES --------------- *)

(* Lemma 16 *)
Lemma dc_exch:
forall (D G G': ctx) (A B C: Formula),
    D|((G,A,B);G')|-s C -> D|((G,B,A);G')|-s C.
(* The proof goes by a nested structural induction on the formulas A, B and the premise. 
This is not yet formalized *)
Admitted.

Lemma dc_exch_vhyps:
forall (D D' G: ctx) (A B C: Formula),
    ((D,A,B);D')|G |-s C -> ((D,B,A);D')|G |-s C.
(* The proof goes by a nested structural induction on the formulas A, B and the premise. 
This is not yet formalized *)
Admitted.


(* Lemma 17 *)
Lemma dc_weakeningT : 
  forall (D: ctx) (G: ctx) (A: Formula),
  D|G|-s A -> forall (C : Formula), D|(G, C)|-s A.
Proof.
intros.
dependent induction H; eauto with GS4 context.
- rewrite ctx_snoc_conc.
  apply dc_thyp.
- apply dc_impR.
  rewrite <- (ctx_conc_empty ((G,C),A)).
  apply dc_exch.
  apply IHDC_Proof.
- rewrite <- ctx_conc_snoc.
  apply dc_disjL.
  rewrite ctx_conc_snoc.
  apply IHDC_Proof1.
  rewrite ctx_conc_snoc.
  apply IHDC_Proof2.
- rewrite <- ctx_conc_snoc.
  apply dc_conjL.
  rewrite ctx_conc_snoc.
  apply IHDC_Proof.
- rewrite <- ctx_conc_snoc.
  apply dc_impL.
  rewrite ctx_conc_snoc.
  apply IHDC_Proof1.
  apply IHDC_Proof2.
- rewrite <- ctx_conc_snoc.
  apply dc_boxL.
  rewrite ctx_conc_snoc.
  apply IHDC_Proof.
- rewrite <- ctx_conc_snoc.
  apply dc_diaL.
  assumption.
- apply dc_disjLV. 
  -- rewrite <- (ctx_conc_empty ((G,C0),A)).
  apply dc_exch.
  apply IHDC_Proof1.
  -- rewrite <- (ctx_conc_empty ((G,C0),B)).
  apply dc_exch.
  apply IHDC_Proof2.
- apply dc_impLV.
  apply IHDC_Proof1.
  rewrite <- (ctx_conc_empty ((G,C0),B)).
  apply dc_exch.
  apply IHDC_Proof2.
Qed. 

Global Hint Resolve dc_weakeningT : GS4.


Lemma dc_weakeningV : 
  forall (D: ctx) (G: ctx) (A: Formula),
  D|G|-s A -> forall (C : Formula), (D, C)| G|-s A.
Proof.
intros.
dependent induction H; eauto with GS4 context.
- rewrite ctx_snoc_conc.
  apply dc_vhyp.
- apply dc_boxL.
  rewrite <- (ctx_conc_empty ((D,C),A)).
  apply dc_exch_vhyps.
  apply IHDC_Proof.
- rewrite ctx_snoc_conc.
  apply dc_disjLV.
  -- apply IHDC_Proof1.
  -- apply IHDC_Proof2.
- rewrite ctx_snoc_conc. 
  apply dc_conjLV.
  apply IHDC_Proof.
- rewrite ctx_snoc_conc.
  apply dc_impLV.
  -- apply IHDC_Proof1.
  -- apply IHDC_Proof2.
- rewrite ctx_snoc_conc.
  apply dc_boxLV. apply IHDC_Proof.
- rewrite ctx_snoc_conc.
  apply dc_diaLV. apply IHDC_Proof.
- eapply dc_cutV.
  apply IHDC_Proof1.
  rewrite <- (ctx_conc_empty ((D,C),A)).
  apply dc_exch_vhyps.
  apply IHDC_Proof2.
Qed.

Global Hint Resolve dc_weakeningV : GS4.


Lemma dc_ctx_weakeningT : 
  forall (D: ctx) (G: ctx) (A : Formula),
  D|G|-s A -> forall (G': ctx), D|(G ; G')|-s A.
Proof.
intros.
induction G'; simpl; intuition.
Qed.

Global Hint Resolve dc_ctx_weakeningT : GS4.

Lemma dc_ctx_weakeningV : 
  forall (D: ctx) (G: ctx) (A : Formula),
  D|G|-s A -> forall (D': ctx), (D ; D')|G|-s A.
Proof.
intros.
induction D'; simpl; intuition.
Qed.

Global Hint Resolve dc_ctx_weakeningV : GS4.


(* Lemma 18 *)
Lemma dc_intro_inv:
forall (D G: ctx) (A B: Formula),
    D|G|-s (A ==> B) -> D|(G,A)|-s B.
Proof.
intros.
dependent induction H; eauto with GS4 context.
- change (((G, (A ==> B)); G'), A) with ((G, (A ==> B)); (G', A)).
apply dc_impL; intuition.
- rewrite ctx_snoc_conc.
apply dc_disjL.
  apply IHDC_Proof1; reflexivity.
  apply IHDC_Proof2; reflexivity.
- rewrite ctx_snoc_conc.
  apply dc_conjL.
  apply IHDC_Proof; reflexivity.
- rewrite ctx_snoc_conc.
  apply dc_impL; simpl.
  -- apply dc_weakeningT. assumption.
  -- apply IHDC_Proof2. reflexivity.
- rewrite ctx_snoc_conc.
  apply dc_boxL.
  simpl. apply IHDC_Proof. reflexivity.
- apply dc_disjLV; simpl. 
  -- rewrite <- (ctx_conc_empty ((G,A),A0)).
  apply dc_exch.
  apply IHDC_Proof1; reflexivity.
  -- rewrite <- (ctx_conc_empty ((G,A),B0)).
  apply dc_exch.
  apply IHDC_Proof2; reflexivity.
- apply dc_impLV; simpl.
  -- apply dc_weakeningT. assumption.
  -- rewrite <- (ctx_conc_empty ((G,A),B0)).
  apply dc_exch.
  apply IHDC_Proof2; reflexivity.
Qed. 
(* 
Proof using cut 
change (G,A) with (G;(empty,A)).
rewrite <- (ctx_conc_empty D).
apply (dc_cut _ _ _ _ (A==>B)).
assumption.
rewrite <- (ctx_conc_empty ((empty, A), (A ==> B))).
apply dc_impL.
apply dc_elem_thyps; intuition.
apply dc_elem_thyps; intuition.
*)
Global Hint Resolve dc_intro_inv : GS4.


Lemma dc_weakeningTG : 
  forall (D: ctx) (G G': ctx) (A B: Formula),
  D|(G ; G')|-s A -> D|(G, B ; G')|-s A.
Proof.
intros.
dependent induction G'.
- rewrite ctx_conc_empty.
  rewrite ctx_conc_empty in H.
  apply dc_weakeningT.
  assumption.
- change (G; (G', f)) with (((G;G'), f);empty) in H.
apply dc_impR in H. 
eapply IHG' in H. 
apply dc_intro_inv in H.
simpl.
exact H.
Qed.

Global Hint Resolve dc_weakeningT : GS4.

Lemma dc_conjL_inv: 
  forall (D: ctx) (G G': ctx) (A B C : Formula),
  DC_Proof D ((G,(A & B));G') C -> DC_Proof D ((G,A,B);G') C.
Proof.
intros.
dependent induction H; eauto with GS4 context.
- symmetry in x. apply ctx_decomposition_conc in x; intuition.
destruct H; auto with context GS4. 
subst. intuition.
- apply dc_impR. 
  rewrite <- ctx_conc_snoc.
  apply IHDC_Proof.
  reflexivity.
- change ((G0, (A0 \ B0)); G'0) with (G0; (empty, (A0 \ B0)); G'0) in x.
rewrite <- ctx_conc_conc in x.
apply ctx_decomposition_conc in x. 
 admit.
- admit.
- admit.
- admit.
- admit.
- admit.
- admit.
Admitted.

Lemma dc_conjLV_inv:
  forall (D D':ctx) (G: ctx) (A B C: Formula),
  DC_Proof ((D,(A & B));D') G C -> DC_Proof ((D,A,B);D') G C.
Proof.
intros.
dependent induction H; auto with GS4 context.
- symmetry in x. apply ctx_decomposition_conc in x; intuition.
destruct H; auto with context GS4. 
subst. intuition.
- apply dc_boxL.
rewrite <- ctx_conc_snoc.
apply IHDC_Proof. 
reflexivity.
- admit.
- admit.
- admit.
- admit.
- admit.
- admit.
Admitted.

Lemma dc_disjL_inv_l:
  forall (D: ctx) (G G': ctx) (A B C: Formula),
  DC_Proof D ((G, (A \ B));G') C -> DC_Proof D ((G,A) ; G') C.
Proof.
intros. 
dependent induction H; eauto with GS4 context.
- symmetry in x. apply ctx_decomposition_conc in x; intuition.
destruct H; auto with context GS4. 
subst. intuition.
- apply dc_impR.
  rewrite <- ctx_conc_snoc.
  eapply IHDC_Proof. intuition.
- admit.
- admit.
- admit.
- admit.
- admit.
- admit.
- admit.
Admitted.

Lemma dc_disjL_inv_r:
  forall (D: ctx) (G G': ctx) (A B C: Formula),
  DC_Proof D ((G, (A \ B));G') C -> DC_Proof D ((G,B) ; G') C.
Admitted.
  

Lemma dc_impL_inv:
  forall (D: ctx) (G G': ctx) (A B C: Formula),
  DC_Proof D ((G,(A==>B));G') C -> DC_Proof D ((G,B);G') C.
Admitted.

Lemma dc_boxL_inv:
forall (D: ctx) (G G': ctx) (A B: Formula),
    D|((G,#A);G')|-s B -> (D,A)|(G;G')|-s B.
Proof.
intros.
dependent induction H; auto with GS4 context.
- symmetry in x.
apply (ctx_decomposition_conc G0 G'0 (G,(#A)) G' A0) in x.
destruct x.
-- destruct H; intuition.
    rewrite <- H. intuition.
-- apply dc_elem_thyps; intuition.
- admit.
Admitted.

Lemma dc_boxLV_inv:
  forall (D D':ctx) (G: ctx) (A B : Formula),
  DC_Proof ((D,#A);D') G B -> DC_Proof ((D,A);D') G B.
Proof.
intros.
dependent induction H; auto with GS4 context.
- symmetry in x. apply ctx_decomposition_conc in x. destruct x.
-- destruct H. 
    subst. intuition.
    eapply dc_elem_vhyps in H.
    admit.
-- intuition.
- admit.
- admit.
Admitted.

(*
(* Extras *)
Lemma dc_impE:
forall (D G: ctx) (A B:Formula), (D|G|-s (A==>B)) -> (D|G|-s A) -> D|G|-s B.
Proof.
intros.
rewrite <- (ctx_conc_empty D).
rewrite <- (ctx_conc_empty G).
apply (dc_cut _ _ _ _ (A==>B) B).
assumption.
rewrite <- (ctx_conc_empty (empty, (A ==> B))).
apply dc_impL; intuition.
admit.
Admitted.


Lemma dc_boxE:
forall (D G:ctx) (A C:Formula), (D|G|-s (#A)) -> ((D,A)|G|-s C) -> D|G|-s C.
Proof.
intros.
rewrite <- (ctx_conc_empty D).
rewrite <- (ctx_conc_empty G).
apply (dc_cut _ _ _ _ (#A) C).
- assumption.
- rewrite <- (ctx_conc_empty (empty, (# A))).
  apply dc_boxL.
  admit.
Admitted.
*)


Lemma dc_weakeningVG : 
  forall (D D': ctx) (G: ctx) (A B: Formula),
  (D ; D')|G |-s A -> (D, B ; D')|G |-s A.
Proof.
intros.
dependent induction D'; auto with context GS4.
- simpl in H.
  rewrite <- (ctx_conc_empty G) in H.
 apply dc_boxL in H.
 eapply IHD' in H.
 simpl. rewrite <- (ctx_conc_empty G).
apply dc_boxL_inv.
exact H.
Qed.


Global Hint Resolve dc_weakeningVG : GS4.


(* Proposition 22 *)
(* Admissibility of contraction
   Both rules are proved simultaneously by a 
   nested structural induction on the contracted formula and the premise. 
   These are not formalized. *)
Proposition dc_contraction:
  forall (D : ctx) (G G' :ctx) (A C: Formula),
  D|(G,A,A);G'|-s C -> D|(G,A);G'|-s C.
Admitted.

Proposition dc_contractionV:
  forall (D D' : ctx) (G :ctx) (A C: Formula),
  (D,A,A);D'|G |-s C -> (D,A);D'|G|-s C.
Admitted.

Lemma dc_impR_gen:
  forall (D: ctx) (G G': ctx) (A B: Formula),
  DC_Proof D ((G,A);G') B -> DC_Proof D (G;G') (A ==> B).
Proof.
intros D G G'.
induction G'.
intuition.
intros.
simpl in H.
apply dc_impR in H.
apply IHG' in H.
apply dc_impR.
change ((G; (G', f)), A) with ((((G; G'), f), A);empty).
apply dc_exch.
intuition.
Qed.

Global Hint Resolve dc_impR_gen : GS4.


Lemma dc_boxL_gen:
  forall (D D': ctx) (G G': ctx) (A B: Formula),
    DC_Proof ((D,A);D') (G;G') B -> DC_Proof (D;D') ((G,#A);G') B.
Proof.
intros D D'.
induction D'.
intuition.
intros.
simpl in H.
simpl.
apply dc_boxL_inv.
change (((G, (# A)), (# f)); G') with (((G, (# A)); (empty,# f)); G').
rewrite <- ctx_conc_conc.
apply IHD'.
apply dc_boxL in H.
rewrite ctx_conc_conc.
simpl.
exact H.
Qed.

Global Hint Resolve dc_boxL_gen : GS4. 


(* Proposition 23 *)
Proposition dc_ctx_contraction:
  forall (D : ctx) (G :ctx) (C: Formula),
  D|G;G|-s C -> D|G|-s C.
Proof.
intros D G.
induction G.
- intuition.
- intros.
simpl in H.
apply dc_impR in H.
  apply dc_impR_gen in H.
  apply IHG in H.
  apply dc_intro_inv in H.
    apply dc_intro_inv in H.
  rewrite <- (ctx_conc_empty ((G,f),f)) in H.
    apply dc_contraction in H.
    intuition.
Qed.
  
Global Hint Resolve dc_ctx_contraction : GS4.


Proposition dc_ctx_contractionV:
  forall (D : ctx) (G :ctx) (C: Formula),
  D;D|G |-s C -> D|G|-s C.
Proof.
intro. 
induction D; intuition.
rewrite <- (ctx_conc_empty G) in H.
apply dc_boxL_gen in H.
simpl in H.
rewrite <- (ctx_conc_empty (G,#f)) in H.
apply dc_boxL in H.
apply IHD in H; intuition.
apply dc_contraction in H.
apply dc_boxL_inv in H.
intuition.
Qed. 

Global Hint Resolve dc_ctx_contractionV : GS4.


(* Proposition 24 *)
Lemma scontraction:
  forall (D D':ctx) (G G' :ctx) (A C: Formula),
  ((D,A);D'|(G,A);G'|-s C) ->((D,A);D'|(G;G')|-s C).
Admitted.
(* The proof is by a nested structural induction on the premise
    and the contracted formula A.
   This is not formalized *)

(* Cut Elimination*)
Theorem dc_cut : 
forall (D D':ctx) (G G': ctx) (A B : Formula),
    DC_Proof D G A -> DC_Proof D' (G',A) B -> DC_Proof (D;D') (G;G') B.
Admitted.
(* The proof is by a simultaneous and triple nested structural induction on the cut formula A and both premises.
This is not formalized. *)

