(** Hilbert style System HK4 from
      Sara Negri & Raul Hakli, 
      Does the deduction theorem fail for modal logic?,
      Synthese 187 (2012), pp.849--867.
*)

Require Import Coq.Program.Equality.
Require Import ModalLogic.
Require Import Context.

(** --------------- INFERENCE RULES --------------- *)
(** System HK for modal logic with local hypotheses, 
  without negation but extended with conjunction, disjunction,
  modal axioms for T and 4, and 
  the corresponding axioms for the possibility modality.
  As remarked by Hakli and Negri,
  in the original system there is no substitution rule, 
  the axiom schemata is used to perform implicit substitutions 
  giving instances of axioms whenever it is needed.
*)


Inductive Deriv: ctx -> Formula -> Prop:=
| Hyp:    forall (G : ctx) (A: Formula), 
          elem A G -> Deriv G A 
      
| Ax1:    forall (G: ctx) (A B: Formula), 
          Deriv G (A ==> (B ==> A))
       
| Ax2:    forall (G: ctx) (A B: Formula), 
          Deriv G ((A ==> (A ==> B)) ==> (A ==> B))
       
| Ax3:    forall (G: ctx) (A B C: Formula), 
          Deriv G ((A ==> (B ==> C)) ==> (B ==>(A ==> C)))
       
| Ax4:    forall (G: ctx) (A B C: Formula), 
          Deriv G ((B ==> C) ==> ((A ==> B) ==> (A ==> C)))

| Ax6:  forall (G: ctx) (A B: Formula), 
          Deriv G ((A & B) ==> A)

| Ax7:  forall (G: ctx) (A B: Formula), 
          Deriv G ((A & B) ==> B)

| Ax5:  forall (G: ctx) (A B: Formula), 
          Deriv G (A ==> (B ==> (A & B)))

| Ax8:  forall (G: ctx) (A B: Formula), 
          Deriv G (A ==> (A \ B))

| Ax9:  forall (G: ctx) (A B: Formula), 
          Deriv G (B ==> (A \ B))

| Ax10:  forall (G: ctx) (A B C: Formula), 
          Deriv G ((A ==> C) ==> ((B==>C) ==>(A \ B) ==> C))

| AxBoxK: forall (G: ctx) (A B: Formula), 
          Deriv G (Box(A ==> B) ==> ((Box A) ==> (Box B)))
        
| AxBoxT: forall (G : ctx) (A : Formula),
          Deriv G ((Box A) ==> A)
       
| AxBox4: forall (G : ctx) (A : Formula),
          Deriv G ((Box A) ==> (Box(Box A)))
       
| AxDiaT: forall (G: ctx) (A: Formula),
          Deriv G (A ==> Dia A)

| AxDiaK: forall (G: ctx) (A B: Formula),
          Deriv G (Box (A ==> B) ==> (Dia A ==> Dia B))

| AxDia4: forall (G: ctx) (A: Formula),
          Deriv G ((Dia (Dia A)) ==> Dia A)

| MP:     forall (G G': ctx) (A B: Formula),
          Deriv G A -> Deriv G' (A ==> B) -> Deriv (G';G) B
      
| Nec:    forall (G: ctx) (A: Formula), 
          Deriv empty A -> Deriv G (Box A).
          
Global Hint Constructors Deriv : HK4.

Notation "G |- A" := (Deriv G A) (at level 30).


(** 
 Verification of statements in the article and other useful lemmas, 
 some of them are the dettached versions of the axioms.
 *)

Lemma AxI: 
  forall (G:ctx) (A:Formula), G |- (A ==> A).
Proof.
intros.
assert (H := Ax1 empty A A).
assert (H1:= Ax2 G A A).
rewrite <- (ctx_conc_empty G).
eapply MP in H1.
- exact H1.
- exact H.
Qed.

Global Hint Resolve AxI : HK4.


Lemma Ax3_dett: 
  forall (G:ctx) (A B C:Formula),
  G |- (A ==> B ==> C) -> G |- (B ==> A ==> C).
Proof.
intros.
rewrite <- (ctx_empty_conc G).
eapply MP.
- exact H.
- apply Ax3.
Qed.

Global Hint Resolve Ax3_dett : HK4.

Lemma transitivity:
  forall (G: ctx) (A B C: Formula),
  G |- ((A ==> B) ==> ((B ==> C) ==> (A==> C))).
Proof.
intros.
rewrite <- (ctx_empty_conc G).
eapply MP.
2: eapply Ax3.
intuition.
Qed.

Global Hint Resolve transitivity : HK4.


Lemma trans_dett:
  forall (G G': ctx)(P Q R : Formula),
  G |- (P ==> Q) -> G' |- (Q ==> R) -> (G;G') |- (P ==> R).
Proof.
intros.
assert (K:= transitivity empty P Q R).
rewrite <- (ctx_empty_conc G).
eapply MP.
- exact H0.
- eapply MP.
  exact H.
  assumption.
Qed.

Global Hint Resolve trans_dett : HK4.

Lemma Ax4_dett:
  forall (G G': ctx) (A B C: Formula),
  G|- (B ==> C) -> G'|- (A ==> B) -> (G;G')|-(A ==> C).
Proof.
intros.
assert (empty|- ((B ==> C) ==> ((A ==> B) ==> (A ==> C)))); intuition.
rewrite <- (ctx_empty_conc G).
eapply MP.
exact H0.
eapply MP.
exact H.
assumption.
Qed.

Global Hint Resolve Ax4_dett : HK4.

(* ----------------------------- 
  Tactic for solving axioms in DeductionTh 
*)
Ltac solve_axioms :=
match goal with 
  | |- (Deriv (conc empty ?G) (Impl ?A ?F)) =>
      eapply (MP _ _ F _); intuition
end.


(* Deduction Theorem *)
Theorem DeductionTh: 
  forall (G: ctx) (A B: Formula), (G,A) |- B -> G |- (A ==> B).
Proof.
intros G A B H.
dependent induction H; rewrite <- (ctx_empty_conc G).
(* Hyp case *)
{ apply elem_inv in H.
  destruct H.
  + rewrite H.
    apply AxI.
  + eapply (MP _ _ A0 (A==> A0)); intuition. }
(* Axioms *)
1-16: solve_axioms.
(* MP case *)
{ rewrite ctx_empty_conc.
  assert (x':=x).
  apply ctx_decomposition in x.
  destruct x; destruct H1.
  + rewrite H1 in H.
    apply IHDeriv2 in H2.
    apply Ax3_dett in H2.
    rewrite <- (ctx_conc_empty G).
    apply (MP _ _ A0 (A ==>B)); intuition.
  + rewrite H1 in x'.
    simpl in x'.
    inversion x'.
    assert (empty |- ((A0 ==> B) ==> (A ==>A0) ==> (A ==> B))); intuition.
    apply (MP G' empty (A0 ==> B) ((A ==> A0) ==> A ==> B) H0) in H2.
    apply IHDeriv1 in H1.
    eapply MP.
    exact H1.
    rewrite (ctx_empty_conc G') in H2.
    exact H2. }
(* Nec case *)
{ eapply MP. 
  + apply Nec.
    exact H.
  + apply Ax1. }
Qed. 

Global Hint Resolve DeductionTh : HK4.


Corollary multihyp_discharge:
  forall (n:nat) (G: ctx) (A B: Formula),
  G;(replicate A n) |- B -> G |- (A==>B).
Proof.
intros.
dependent induction n; simpl in H ; rewrite <- (ctx_empty_conc G). 
- eapply (MP _ _ B (A ==> B)).
  + assumption.
  + apply Ax1.
- apply DeductionTh in H.
  apply IHn in H.
  eauto with HK4.
Qed.

Global Hint Resolve multihyp_discharge : HK4.


(* Corollary 2 Substitution (cut) or closure under composition *)
Corollary substitution:
  forall (G G': ctx) (A B: Formula),
  (G |- A) -> (G',A |- B) -> (G';G |- B).
Proof.
intros.
apply DeductionTh in H0.
eapply MP.
- exact H.
- exact H0.
Qed.

Global Hint Resolve substitution : HK4.


(* Theorem Inverse Deduction Theorem or Principle of Detachment *)
Theorem inverseDT:
  forall (G: ctx) (A B: Formula), G |- (A ==> B) -> G,A |- B.
Proof.
intros.
assert ((empty,A)|- A); intuition.
change (G,A) with (G; (empty,A)).
eapply MP.
exact H0.
exact H.
Qed.

Global Hint Resolve inverseDT : HK4.

(* Generalized Deduction Theorem *)
Theorem deductionTh_genPremise: 
  forall (G' G: ctx) (A B: Formula), (G,A);G' |- B -> G;G' |- (A ==> B).
Proof.
intro.
induction G' ; auto ; intros.
+ 
simpl in H.
apply DeductionTh in H.
intuition.
+
simpl  in H.
apply DeductionTh in H.
apply IHG' in H.
apply Ax3_dett in H.
apply inverseDT in H.
intuition.
Qed.

Global Hint Resolve deductionTh_genPremise : HK4.


(* Lemma 3 Context Permutation *)
Lemma ctx_permutation: 
  forall (G G':ctx) (A:Formula), G;G' |- A -> G';G |- A.
Proof.
intro G.
induction G.
- intros.
  simpl.
  rewrite ctx_empty_conc in H.
  assumption.
- intros.
  simpl.
  apply inverseDT.
  apply IHG.
  apply deductionTh_genPremise in H.
  assumption.
Qed.

Global Hint Resolve ctx_permutation : HK4.

Theorem inverseDT_genPremise:
  forall (G' G: ctx) (A B: Formula), G;G' |- (A ==> B) -> (G,A);G' |- B.
Proof.
intros.
apply ctx_permutation in H.
apply inverseDT in H.
apply ctx_permutation.
intuition.
Qed.

Global Hint Resolve inverseDT_genPremise : HK4.

Lemma ctx_permutation_gen: 
  forall (G'' G' G:ctx) (A:Formula), (G;G');G'' |- A -> (G';G);G'' |- A.
Proof.
intro.
induction G''; intros.
- simpl. simpl in H. apply ctx_permutation; intuition.
- simpl. apply inverseDT. 
  simpl in H. apply DeductionTh in H.
  apply IHG''; intuition.
Qed.

Global Hint Resolve ctx_permutation_gen : HK4.

Corollary contraction_hyp:
  forall (G : ctx) (A B : Formula), (G, A), A |- B -> G, A |- B.
Proof.
intros.
apply inverseDT.
repeat apply DeductionTh in H.
assert(K:= Ax2 empty A B).
rewrite <- (ctx_empty_conc G).
eapply MP.
exact H.
assumption.
Qed.

Global Hint Resolve contraction_hyp : HK4.


Lemma Ax2_dett: 
  forall (G:ctx) (A B:Formula), G |- (A ==> A ==> B) -> G |- (A ==> B).
Proof.
intros.
auto with HK4.
Qed.

Global Hint Resolve Ax2_dett : HK4.


(* Lemma 3 Context Contraction *)
Lemma ctx_contraction: 
  forall (G:ctx) (A:Formula), G;G |- A -> G |- A.
Proof.
intro.
induction G.
- auto.
- intros.
  apply deductionTh_genPremise in H.
  simpl in H.
  apply DeductionTh in H.
  apply Ax2_dett in H.
  apply inverseDT.
  apply IHG.
  assumption.
Qed.

Global Hint Resolve ctx_contraction : HK4.


Lemma ctx_contraction_gen: 
  forall (G G' G'':ctx) (A:Formula), G';(G;G);G'' |- A -> G';G;G'' |- A.
Proof.
intro.
induction G; intros; auto.

rewrite <- (ctx_snoc_conc G G f f) in H.
simpl in H.
apply deductionTh_genPremise in H.
rewrite (ctx_conc_conc G' (G, f) G) in H.
apply ctx_permutation_gen in H.
simpl in H.
apply deductionTh_genPremise in H.
apply ctx_permutation_gen in H.
rewrite <- (ctx_conc_conc G' G G) in H.

apply IHG in H.
apply Ax2_dett in H.
simpl.
apply inverseDT_genPremise.
intuition.
Qed.

Global Hint Resolve ctx_contraction_gen : HK4.



Lemma AxBoxK_dett: 
  forall (G:ctx) (A B:Formula), (G |- #(A ==> B)) -> G|- (#A ==> #B).
Proof.
intros.
rewrite <- (ctx_empty_conc G).
eapply MP.
- exact H.
- apply AxBoxK.
Qed.

Global Hint Resolve AxBoxK_dett : HK4.

Lemma AxDiaK_dett:
  forall (G: ctx) (A B: Formula), (G |- #(A ==> B)) -> G |- ($A ==> $B).
Proof.
intros.
eauto with HK4.
Qed.

Global Hint Resolve AxDiaK_dett : HK4.


Lemma weakening:
  forall (G: ctx) (A: Formula), (G|- A) -> forall (G':ctx), G;G'|-A.
Proof.
intros G A H.
dependent induction H; simpl; auto with HK4 context.
intro. 

case_eq G'0; intro; simpl.
eapply MP. 
apply (IHDeriv1 empty); intuition.
apply (IHDeriv2 empty); intuition.

intros.
rewrite <- ctx_conc_conc.
change ((G'; (G; c)), f) with (G'; ((G; c), f)).
eapply MP. 
change ((G; c), f) with (G; (c, f)).
apply (IHDeriv1 ); intuition.
apply (IHDeriv2 empty); intuition.
Qed.

Global Hint Resolve weakening : HK4.

Lemma elem_hyps: 
  forall (G : ctx) (A: Formula), elem A G -> G |- A. 
Proof.
intros.
auto with HK4 context.
Qed.

Global Hint Resolve elem_hyps : HK4.


Lemma weakening_gen:
  forall (G G': ctx) (A: Formula), (G;G'|- A) -> forall (B: Formula), (G,B);G'|-A.
Proof.
intros G G' A H.
induction G'.
+ intro.
  change ((G,B); empty) with (G;(empty,B)).
  apply weakening.
  intuition.
+ intro.
  change ((G, B); (G', f)) with (((G;(empty, B)); G'), f).
  apply inverseDT.
  apply ctx_permutation_gen.
  rewrite <- ctx_conc_conc.
  apply ctx_permutation.
  apply weakening.
  intuition.
Qed.

Global Hint Resolve weakening_gen : HK4.


Lemma boxtrans: 
  forall (G:ctx) (A B:Formula), 
  G |- (#(#A) ==> B) ->  G |- (#A ==> B).
Proof.
intros.
assert(K:=AxBox4 empty A).
rewrite <- (ctx_empty_conc G).
eapply trans_dett.
exact K.
exact H.
Qed.

Global Hint Resolve boxtrans : HK4.


Lemma GenNec_swap: 
  forall (D:ctx) (A:Formula),
   boxed D |- A -> forall (G:ctx), G; boxed D |- Box A.
Proof.
intro.
induction D; auto with HK4.
intros.
simpl in H.
apply DeductionTh in H.
eapply IHD in H.
apply AxBoxK_dett in H.
apply boxtrans in H.
apply inverseDT in H.
exact H.
Qed.

Global Hint Resolve GenNec_swap : HK4.


(** Modal introduction rules *)
(* Lemma 4 Diamond Introduction *)
Lemma intro_dia: 
  forall (D:ctx) (A:Formula),
  D |- A -> D |- ($A).
Proof.
intros.
apply ctx_contraction.
eapply MP.
exact H.
apply AxDiaT.
Qed.

Global Hint Resolve intro_dia : HK4.


(* Lemma 4 General Neccesitation *)
Lemma GenNec: 
  forall (D:ctx) (A:Formula),
   boxed D |- A -> forall (G:ctx), boxed D; G |- Box A.
Proof.
intro.
induction D; intuition.
Qed.

Global Hint Resolve GenNec : HK4.

(** Other useful derivable rules *)
Lemma elim_dia:
  forall (D G: ctx) (A B: Formula),
  (boxed D);G |- ($A) ->   (boxed D),A |- ($B) ->   (boxed D);G |- ($B).
Proof.
intros. 
apply DeductionTh in H0.
eapply GenNec in H0.
apply AxDiaK_dett in H0.
apply (MP _ _ _ _ H) in H0.
apply ctx_contraction in H0.
assert (K:= AxDia4 (boxed D; G)  B).
apply (MP _ _ _ _ H0) in K.
apply ctx_contraction in K.
assumption.
Qed.

Global Hint Resolve elim_dia : HK4.


Lemma box_dia:
  forall (G: ctx) (A B: Formula), G |- (#A ==>  $A).
Proof.
intros.
apply ctx_contraction.
apply (trans_dett _ _ (#A) A ($A)); intuition.
Qed.

Global Hint Resolve box_dia : HK4.



(** Example 3.1 *)

Proposition deriv_example:
  forall (A B: Formula), empty |- ((#(A ==> B)) ==> $(#A ==> $B)).
Proof.
intros.
apply DeductionTh.
apply intro_dia.
apply DeductionTh.
change ((empty, (# (A ==> B))), (# A)) with ((empty, (# (A ==> B))); (empty, (# A))).
apply (MP _ _ ($A)).
- rewrite <- (ctx_empty_conc (empty,#A)).
apply (MP _ _ (#A)).
apply elem_hyps; intuition.
(* differs from the example in article as here the proof is save up by a derivable rule *)
apply box_dia; intuition.
- apply AxDiaK_dett; intuition.
Qed.
