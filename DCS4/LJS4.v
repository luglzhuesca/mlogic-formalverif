(** Sequent Calculus for proof search *)

Require Import Coq.Program.Equality.
Require Import ModalLogic.
Require Import Context.
Require Export HS4.


(** --------------- INFERENCE RULES --------------- *)
(** There is only one formula in the right hand side of the sequent.
The left rule for implication used here emphasizes the proof-search.
The structural rules for weakening, contraction and permutation are not primitive. *)
Inductive LJ_S4 : ctx -> Formula -> Prop :=
| lj_i : forall (G G': ctx) (A: Formula),
             LJ_S4 ((G,A);G') A

| lj_cut :  forall (G G': ctx) (A B: Formula),
            LJ_S4 G A -> LJ_S4 (G',A) B -> 
            LJ_S4 (G;G') B

| lj_conjL : forall (G G': ctx) (A B C: Formula),
            LJ_S4 ((G, A, B); G') C -> 
            LJ_S4 ((G,(A & B));G') C

| lj_conjR : forall (G: ctx) (A B: Formula),
            LJ_S4 G A -> LJ_S4 G B ->
            LJ_S4 G (A & B)

| lj_disjL : forall (G G': ctx) (A B C: Formula),
            LJ_S4 ((G,A);G') C ->  LJ_S4 ((G,B);G') C ->
            LJ_S4 ((G,(A \ B));G') C

| lj_disjR_l : forall (G: ctx) (A B: Formula),
            LJ_S4 G A -> 
            LJ_S4 G (A \ B)

| lj_disjR_r : forall (G: ctx) (A B: Formula),
            LJ_S4 G B ->
            LJ_S4 G (A \ B)

| lj_impL : forall (G G': ctx) (A B C: Formula),
            LJ_S4 ((G,(A ==> B));G') A -> LJ_S4 ((G,B);G') C -> 
            LJ_S4 ((G,(A ==> B));G') C

| lj_impR : forall (G G': ctx) (A B: Formula),
            LJ_S4 ((G, A);G') B -> 
            LJ_S4 (G;G') (A ==> B)

| lj_boxL : forall (G G': ctx) (A B: Formula),
            LJ_S4 (((G,(#A)),A);G') B -> 
            LJ_S4 ((G,(#A));G') B

| lj_boxR : forall (G:ctx) (A: Formula),
            LJ_S4 (boxed G)  A -> 
            LJ_S4 (boxed G) (#A)

| lj_diaL : forall (G G': ctx) (A B : Formula),
            LJ_S4 ((boxed G),A) ($B) -> 
            LJ_S4 (((boxed G),$A);G') ($B)

| lj_diaR : forall (G:ctx) (A: Formula),
            LJ_S4 G A -> 
            LJ_S4 G ($A).

Global Hint Constructors LJ_S4 : LJ.


Lemma lj_elem_hyps: 
  forall (G : ctx) (A: Formula), elem A G -> LJ_S4 G A. 
Proof.
intros.
assert( exists G1, exists G2, G=(G1,A);G2 ).
- apply elem_ctxsplit.
  assumption.
- destruct H0 as [G1'].
  destruct H0 as [G2'].
  rewrite H0.
  apply lj_i.
Qed.

Global Hint Resolve lj_elem_hyps : LJ.


(* Exchange 
Lemma lj_pL : 
  forall (G G': ctx) (A B C: Formula),
  LJ_S4 ((G,A),B;G') C -> LJ_S4 ((G,B),A;G') C.
 *)
 

Lemma lj_detachment: 
  forall (G: ctx) (A B: Formula),
  LJ_S4 G (A ==> B) -> LJ_S4 (G, A) B.
Proof.
intros.
change (G,A) with (G;(empty,A)).
apply (lj_cut _ _ (A==>B)).
assumption.
rewrite <- (ctx_conc_empty ((empty, A), (A ==> B))).
apply lj_impL; intuition.
apply lj_elem_hyps.
simpl. intuition. 
Qed.

Global Hint Resolve lj_detachment : LJ.


Lemma lj_impR_inv: 
  forall (G G': ctx) (A B: Formula),
  LJ_S4 (G;G') (A ==> B) -> LJ_S4 ((G, A);G') B.
Proof.
intros G G'.
induction G'; intros; simpl in *.
intuition.

apply lj_detachment.
apply IHG'.

apply lj_detachment in H.
rewrite <- (ctx_conc_empty (G; G')).

apply lj_impR.
simpl.
change ((G; G'), A) with ((G;G');(empty, A)).
apply lj_impR.
simpl.
assumption.
Qed.

Global Hint Resolve lj_impR_inv : LJ.

Lemma lj_A1:
  forall (G: ctx) (A B: Formula),
  LJ_S4 G (A==>(B==>A)).
Proof.
intros.
rewrite <- (ctx_conc_empty G).
repeat (apply lj_impR).
apply lj_elem_hyps.
intuition.
Qed.

Global Hint Resolve lj_A1 : LJ.
 
Lemma lj_A3:
  forall (G: ctx) (A B: Formula),
  LJ_S4 G ((A==>(A==>B)) ==> (A==>B)).
Proof.
intros.
rewrite <- (ctx_conc_empty G).
repeat (apply lj_impR).
change ((G, (A ==> A ==> B)), A) with ((G, (A ==> A ==> B)); (empty, A)).
apply lj_impL.
- simpl. 
  rewrite <- (ctx_conc_empty ((G, (A ==> A ==> B)), A)).
  intuition.
- simpl.
  apply lj_detachment.
  intuition.
Qed.

Global Hint Resolve lj_A3 : LJ.



Theorem lj_AK: 
  forall (A B:Formula),
  LJ_S4 empty ((#(A ==> B)) ==> ((#A) ==> (#B))).
Proof.
intros.
rewrite <- (ctx_empty_conc empty).
apply lj_impR.
apply lj_impR.
change  ((empty, (# (A ==> B))), (# A)) with ((boxed (empty, (A==>B),A));empty).
apply lj_boxR.

simpl.
rewrite <- (ctx_conc_empty ((empty, (# (A ==> B))), (# A))).

apply lj_boxL.

simpl.
change (((empty, (# (A ==> B))), (# A)),A) with ((empty, (# (A ==> B))); ((empty,# A),A)).

apply lj_boxL.

apply lj_impL; intuition.
Qed.

Global Hint Resolve lj_AK : LJ.


Lemma lj_boxtrans: 
  forall (G:ctx) (A B:Formula), 
  LJ_S4 G ((#(#A) ==> B) ==> (#A ==> B)).
Proof.
intros.
rewrite <- (ctx_conc_empty G).
repeat (apply lj_impR).
simpl.
change ((G, (# # A ==> B)), (# A)) with ((G, (# # A ==> B)); (empty, (# A))).
apply lj_impL; intuition.
simpl.
rewrite <- (ctx_empty_conc ((G, (# # A ==> B)), (# A))).
apply (lj_cut _ _ (#A ==> ##A)).
-- rewrite <- (ctx_conc_empty empty). 
apply lj_impR.
simpl. 
change empty with (boxed empty).
change (boxed empty, (# A)) with (boxed (empty,A)).
apply lj_boxR.
intuition.
-- rewrite <- (ctx_conc_empty  (((G, (# # A ==> B)), (# A)), (# A ==> # # A))).
apply lj_impL; intuition.
Qed.

Global Hint Resolve lj_boxtrans : HK4.

Lemma lj_gen_nec:
  forall (G : ctx) (A: Formula), LJ_S4 (boxed G) A -> 
    forall (G': ctx), LJ_S4 (G'; (boxed G)) (#A).
Proof.
intro.
induction G; intros.
- induction G'.
-- rewrite ctx_empty_conc.
 apply lj_boxR.
 assumption.
-- simpl.
  apply lj_detachment.
  rewrite <- (ctx_conc_empty G').
  apply (lj_cut _ _ (#A)); intuition.
  
- simpl in H.
rewrite <- (ctx_conc_empty (boxed G, (# f))) in H.
apply lj_impR in H.
assert (LJ_S4 (boxed G) (# f ==> A)); intuition.
simpl.
apply lj_detachment.
(* eapply IHG in H. *)
assert (K:= IHG _ H).
rewrite <- (ctx_conc_empty (G';boxed G)).
apply (lj_cut _ _ (#(#f) ==> #A)).
+ rewrite <- (ctx_conc_empty (G';boxed G)).
apply (lj_cut _ _ (#(#f ==> A))).
++ apply IHG. assumption.
++  apply lj_detachment. apply  lj_AK.
+ apply lj_detachment.
  apply lj_boxtrans.
Qed.  




Lemma lj_wL : 
  forall (G: ctx) (A B: Formula),
  LJ_S4 G A -> LJ_S4 (G,B) A.
Proof.
intros.
dependent induction H; simpl; auto with LJ context.
(* - rewrite ctx_snoc_conc in *.
  apply lj_i.
*) 
- (* change ((G; G'), B) with (G;(G', B)) in IHLJ_S4_1. 
  apply (lj_cut _ _ _ _ IHLJ_S4_1) in IHLJ_S4_2.
  intuition.
  *)
  change ((G; G'), B) with (G;(G', B)).
  eapply lj_cut.
  rewrite <- (ctx_conc_empty (G,B)) in IHLJ_S4_1. 
  exact H.
  apply lj_detachment.
  change ((G', A), B) with ((G', A);(empty, B)) in IHLJ_S4_2.
  apply lj_impR in IHLJ_S4_2.
  intuition.
  
  
- apply lj_conjL in H.
  change ((((G, A), B0); G'), B) with (((G, A), B0); (G', B)) in IHLJ_S4.
  apply lj_conjL in IHLJ_S4.
  intuition.

- rewrite ctx_snoc_conc in *.
  auto with LJ. 

- rewrite ctx_snoc_conc in *.
  apply lj_impL; intuition.

- change  ((G; G'), B) with (G;( G', B)). 
  apply lj_impR.
  intuition.

- rewrite ctx_snoc_conc in *. 
  apply lj_boxL.
  assumption.

-
apply lj_detachment.
rewrite <- (ctx_empty_conc (boxed G)).
apply lj_impR.
apply lj_gen_nec.
assumption.

- change ((((boxed G), ($ A)); G'), B) with (((boxed G), ($ A));(G', B)). 
  eapply lj_diaL.
exact H.
Qed.

Global Hint Resolve lj_wL : LJ.


Lemma lj_weak_ctx:
  forall (G: ctx) (A: Formula),
  LJ_S4 G A -> forall (G': ctx), LJ_S4 (G;G') A.
Proof.
intros G A H.
induction G'. 
simpl. intuition.
simpl.
apply lj_wL.
assumption.
Qed.

Global Hint Resolve lj_weak_ctx : LJ.

(* 
Lemma lj_mp:
  forall (G: ctx) (A B : Formula),
  LJ_S4 G (A ==> B) -> LJ_S4 G A -> LJ_S4 G B.
Proof.
intros.
rewrite <- (ctx_conc_empty G).
eapply lj_cut.
exact H.
rewrite <- (ctx_conc_empty (empty, (A ==> B))).
apply lj_impL. 
apply lj_wL. assumption.
apply lj_i.
Qed.

Global Hint Resolve lj_mp : LK. 
 *)

Lemma lj_weak_gen:
  forall (G' G: ctx) (A: Formula),
  LJ_S4 (G;G') A -> forall (B: Formula), LJ_S4 ((G,B);G') A.
Proof.
induction G'.
- simpl; intuition.
- intros. 
change ((G; (G', f))) with (((G; G'), f); empty) in H.
apply lj_impR in H. simpl in H.
eapply IHG' in H.
simpl.
apply lj_detachment.
exact H.
Qed.

Global Hint Resolve lj_weak_gen : LJ.


Lemma lj_weak_ctx_bis:
  forall (G: ctx) (A: Formula),
  LJ_S4 G A -> forall (G': ctx), LJ_S4 (G';G) A.
Proof.
intros G A H.
induction G'.
- rewrite ctx_empty_conc; assumption.
- apply lj_weak_gen.
assumption.
Qed.

Global Hint Resolve lj_weak_ctx_bis : LJ.


Lemma lj_perm_ctx:
  forall (G G': ctx) (A: Formula),
  LJ_S4 (G;G') A -> LJ_S4 (G';G) A.
Proof.
intros G G'.
induction G; intros; simpl. 
- rewrite ctx_empty_conc in H. intuition.
-(*  eauto with LJ. *)
  apply lj_impR in H.
  apply IHG in H.
  rewrite <- (ctx_conc_empty (G';G)) in H.
  apply lj_impR_inv in H.
  assumption.
Qed.

Global Hint Resolve lj_perm_ctx : LJ.


(* Contraction
Lemma lj_cL : 
  forall (G G': ctx) (A B: Formula),
  LJ_S4 (((G,A),A);G') B -> LJ_S4 ((G,A);G') B.
*)


(** 
 Modal Axioms that characterize the logic S4 
 they are derivable in LJ_S4
 *)

Theorem Axiom_T: 
  forall (G: ctx) (A:Formula), LJ_S4 G ((#A) ==> A).
Proof.
intros. rewrite <- (ctx_conc_empty G).
apply lj_impR.
apply lj_boxL.
apply lj_i.
Qed.

Global Hint Resolve Axiom_T : LJ.

(* Lemma rule_T:
  forall (G: ctx) (A:Formula), LJ_S4 G A ->  LJ_S4 (boxed G) A.
Proof.
intro.
induction G; intros.
intuition.
simpl.
rewrite <- (ctx_conc_empty (boxed G, (# f))).
apply lj_impR_inv.
apply IHG.
 rewrite <- (ctx_conc_empty G).
apply lj_impR.
apply lj_boxL.
assumption.
Qed.

Global Hint Resolve rule_T : LJ.
 *)

(* Lemma scott:
  forall (G: ctx) (A:Formula), LJ_S4 G A ->  LJ_S4 (boxed G) (#A).
Proof.
*)

 
Theorem Axiom_K_gen: 
  forall (G: ctx) (A B:Formula),
  LJ_S4 G ((#(A ==> B)) ==> ((#A) ==> (#B))).
Proof.
intros.
rewrite <- (ctx_empty_conc G).
apply lj_impR.
apply lj_impR.
apply lj_weak_ctx.
change  ((empty, (# (A ==> B))), (# A)) with ((boxed (empty, (A==>B),A));empty).
apply lj_boxR.
simpl.
change ((empty, (# (A ==> B))), (# A)) with ((empty, (# (A ==> B))); (empty,# A)).
apply lj_boxL.
simpl.
change (((empty, (# (A ==> B))), (A ==> B)), (# A)) with (((empty, (# (A ==> B)), (A ==> B)), (# A)); empty).
apply lj_boxL.
change (((((empty, (# (A ==> B))), (A ==> B)), (# A)), A); empty) with (((empty, (# (A ==> B))), (A ==> B));(empty, (# A), A)).
apply lj_impL; intuition.
Qed.

Global Hint Resolve Axiom_K_gen : LJ.

Theorem Axiom_4: 
  forall (G: ctx) (A:Formula), LJ_S4 G (#A ==> ##A).
Proof.
intros.
rewrite <- (ctx_empty_conc G).
apply lj_impR.
apply lj_weak_ctx.
change (empty, (# A)) with ((boxed (empty,  A));empty).
apply lj_boxR.
intuition.
Qed.

Global Hint Resolve Axiom_4 : LJ.


Theorem Axiom_D:
  forall (G: ctx) (A: Formula), 
  LJ_S4 G (#A ==> $A).
Proof.
intros.
rewrite <- (ctx_conc_empty G).
auto with LJ.
Qed.

Global Hint Resolve Axiom_D : LJ.


Theorem Axiom_DiaT:
  forall (G: ctx) (A: Formula),
  LJ_S4 G (A ==> $A).
Proof.
intros.
rewrite <- (ctx_conc_empty G).
apply lj_impR.
apply lj_diaR.
apply lj_i.
Qed.

Global Hint Resolve Axiom_DiaT : LJ.


Theorem Axiom_Dia4:
  forall (G: ctx) (A: Formula),
  LJ_S4 G ($$A ==> $A).
Proof.
intros.
rewrite <- (ctx_empty_conc G).
change (empty; G) with ((boxed empty); G).
apply lj_impR.
apply lj_diaL.
intuition.
Qed.

Global Hint Resolve Axiom_Dia4 : LJ. 

Theorem Axiom_DiaK:
  forall (G: ctx) (A B: Formula),
  LJ_S4 G ((#(A ==> B)) ==> ($A ==> $B)).
Proof.
intros.
rewrite <- (ctx_empty_conc G).
change (empty;G) with ((boxed empty);G).
apply lj_impR.
(* apply lj_boxL. *)
apply lj_impR.
change (((boxed empty, (# (A ==> B))), ($ A)); G) with ((boxed (empty, (A ==> B))), ($ A); G).
apply lj_diaL.
apply lj_diaR.
simpl.
change ((empty, (#(A ==> B))), A) with (empty, (#(A ==> B));(empty, A)).
apply lj_boxL.
intuition.
Qed.

Global Hint Resolve Axiom_DiaK : LJ.


Theorem deriv_example:
  forall (A B: Formula), LJ_S4 (empty;empty) ((#(A ==> B)) ==> $(#A ==> $B)).
Proof.
intros.
apply lj_impR.
apply lj_diaR.
apply lj_impR.
apply lj_diaR.
change (((empty, (# (A ==> B))), (# A)); empty) with ((empty, (# (A ==> B))); (empty,# A)).
apply lj_boxL.
apply lj_impL.
rewrite <- (ctx_conc_empty (((empty, (# (A ==> B))), (A ==> B)); (empty, (# A))) ).
apply lj_boxL.
apply lj_i.
apply lj_i.
Qed.