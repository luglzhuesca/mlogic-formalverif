(** Natural Deduction System for necessity from
  ''A judmental reconstruction of Modal Logic'' 
   Pfenning & Davies (2001)
 *)

Require Import Coq.Program.Equality.
Require Import ModalLogic.
Require Import Context.

(** --------------- INFERENCE RULES --------------- *)
(** The distinction between valid and true hypotheses is expressed by 
    separate sets of hypotheses, ie two contexts as arguments for the
    hypothetical judments.  
    This definition incorporates the possibility judgment in 
    dedicated rules with explicit constructors. *)
Inductive JND_Proof : ctx -> ctx -> Judgm -> Prop :=

| jnd_thyp : forall (D: ctx) (G G': ctx) (A: Formula),
             JND_Proof D ((G,A) ; G') (JTrue A)

| jnd_vhyp : forall (D D': ctx) (G: ctx) (B: Formula),
             JND_Proof (D,B ; D') G (JTrue B)

| jnd_intro : forall (D: ctx) (G: ctx) (A B: Formula),
             JND_Proof D (G,A) (JTrue B) -> JND_Proof D G (JTrue (A ==> B))

| jnd_apply : forall (D: ctx) (G: ctx) (A B: Formula),
             JND_Proof D G (JTrue (A ==> B)) -> 
             JND_Proof D G (JTrue A) -> JND_Proof D G (JTrue B)

| jnd_andI : forall (D: ctx) (G: ctx) (A B: Formula),
             JND_Proof D G (JTrue A) -> JND_Proof D G (JTrue B) -> 
             JND_Proof D G (JTrue (Conj A B))

| jnd_andE_l : forall (D: ctx) (G: ctx) (A B: Formula),
             JND_Proof D G (JTrue (A & B)) -> 
             JND_Proof D G (JTrue A)

| jnd_andE_r : forall (D: ctx) (G: ctx) (A B: Formula),
             JND_Proof D G (JTrue (A & B)) -> 
             JND_Proof D G (JTrue B)

| jnd_orI_l : forall (D: ctx) (G: ctx) (A B: Formula),
             JND_Proof D G (JTrue A) -> JND_Proof D G (JTrue (A \ B))
             

| jnd_orI_r : forall (D: ctx) (G: ctx) (A B: Formula),
             JND_Proof D G (JTrue B) ->JND_Proof D G (JTrue (A \ B))

| jnd_orE : forall (D: ctx) (G: ctx) (A B C: Formula),
             JND_Proof D G (JTrue (A \ B)) -> JND_Proof D (G,A) (JTrue C) -> 
             JND_Proof D (G,B) (JTrue C) -> JND_Proof D G (JTrue C)

| jnd_boxI : forall (D: ctx) (G: ctx) (A: Formula),
             JND_Proof D empty (JTrue A) -> JND_Proof D G (JTrue (Box A))

| jnd_boxE : forall (D: ctx) (G: ctx) (A C: Formula),
             JND_Proof D G (JTrue (Box A)) -> JND_Proof (D,A) G (JTrue C) ->
             JND_Proof D G (JTrue C)

| jnd_boxEp : forall (D: ctx) (G: ctx) (A C: Formula),
             JND_Proof D G (JTrue (Box A)) -> JND_Proof (D,A) G (JPoss C) ->
             JND_Proof D G (JPoss C)

| jnd_diaI : forall (D: ctx) (G: ctx) (A: Formula),
             JND_Proof D G (JPoss A) -> JND_Proof D G (JTrue (Dia A))

| jnd_diaE : forall (D: ctx) (G: ctx) (A C: Formula),
             JND_Proof D G (JTrue (Dia A)) -> JND_Proof D (empty,A) (JPoss C) ->
             JND_Proof D G (JPoss C)

| jnd_tp   : forall (D G: ctx) (A: Formula),
             JND_Proof D G (JTrue A) ->  JND_Proof D G (JPoss A).

Global Hint Constructors JND_Proof : CS4.


(** Example 4.1 *)

Proposition deriv_example:
  forall (A B: Formula), 
  JND_Proof empty empty (JTrue ((#(A ==> B)) ==> $(#A ==> $B))).
Proof.
intros.
apply jnd_intro.
apply jnd_diaI.
apply (jnd_boxEp empty (empty,#(A==>B)) (A==>B)  (#A ==> $B)).
rewrite <- (ctx_conc_empty (empty,#(A==>B))).
apply jnd_thyp.
apply jnd_tp.
apply jnd_intro.
apply jnd_diaI.
apply jnd_tp.
apply (jnd_boxE (empty, (A ==> B)) ((empty, (# (A ==> B))), (# A)) A B).
rewrite <- (ctx_conc_empty ((empty, (# (A ==> B))), (# A)) ).
apply jnd_thyp.
apply (jnd_apply ((empty, (A ==> B)), A) ((empty, (# (A ==> B))), (# A)) A B).
rewrite <- (ctx_conc_empty (empty, (A ==> B))).
rewrite (ctx_snoc_conc empty empty (A ==> B) A).
apply jnd_vhyp.
rewrite <- (ctx_conc_empty ((empty, (A ==> B)), A)).
apply jnd_vhyp.
Qed.



(**  Conclude a hypothesis *) 
Lemma jnd_elem_thyps: 
  forall (D: ctx) (G : ctx) (A: Formula), elem A G -> JND_Proof D G (JTrue A).
Proof.
intros.
assert( exists G1, exists G2, G=(G1,A);G2 ).
- apply elem_ctxsplit.
  assumption.
- destruct H0 as [G1'].
  destruct H0 as [G2'].
  rewrite H0.
  apply jnd_thyp.
Qed.

Global Hint Resolve jnd_elem_thyps : CS4.

Lemma jnd_thyp_last: 
  forall (D G:ctx) (A:Formula), JND_Proof D (G,A) (JTrue A).
Proof.
intros.
rewrite <- (ctx_conc_empty (G,A)).
apply jnd_thyp.
Qed.

Global Hint Resolve jnd_thyp_last : CS4.


Lemma jnd_elem_vhyps: 
  forall (D: ctx) (G : ctx) (A: Formula), elem A D -> JND_Proof D G (JTrue A).
Proof.
intros.
assert( exists D1, exists D2, D=(D1,A);D2 ).
- apply elem_ctxsplit.
  assumption.
- destruct H0 as [D1'].
  destruct H0 as [D2'].
  rewrite H0.
  apply jnd_vhyp.
Qed.

Global Hint Resolve jnd_elem_vhyps : CS4.


(** --------------- STRUCTURAL RULES --------------- *)

(* Weakening *)
Lemma jnd_weakening_thyps: 
  forall (D: ctx) (G G': ctx) (A: Judgm),
  JND_Proof D (G ; G') A -> forall (B : Formula), JND_Proof D (G, B ; G') A.
Proof.
intros.
dependent induction H; eauto with CS4.
- apply jnd_elem_thyps.
  assert(elem A (G;G')).
  + rewrite <- x. 
    intuition.
  + apply elem_conc_split in H.
    destruct H; intuition.
- apply jnd_intro.
  rewrite ctx_snoc_conc.
  apply (IHJND_Proof G (G',A) eq_refl B0).
- eapply jnd_orE; intuition; rewrite ctx_snoc_conc; intuition.
Qed.

Global Hint Resolve jnd_weakening_thyps : CS4.


Lemma jnd_weak_last : 
  forall (D: ctx) (G: ctx) (A : Judgm),
  JND_Proof D G A -> forall (B : Formula), JND_Proof D (G, B) A.
Proof.
intros.
change (G,B) with ((G,B);empty).
apply jnd_weakening_thyps.
simpl.
assumption.
Qed.

Global Hint Resolve jnd_weak_last : CS4. 


Lemma jnd_weakening_vhyps : 
  forall (D D': ctx) (G: ctx) (A: Judgm),
  JND_Proof (D ; D') G A -> forall (B : Formula), JND_Proof (D, B ; D') G A.
Proof.
intros.
dependent induction H; eauto with CS4.
(* - apply jnd_thyp. *)
- apply jnd_elem_vhyps.
  assert(elem B (D;D')).
  + rewrite <-x. intuition.
  + apply elem_conc_split in H.
    destruct H.
    * apply elem_conc_L.
      intuition.
    * apply elem_conc_R. 
    assumption.
(* - apply jnd_intro.
  apply IHJND_Proof.
  intuition.
- eapply jnd_apply.
  + apply IHJND_Proof1; intuition.
  + apply IHJND_Proof2; intuition. 
- apply jnd_boxI.
  apply IHJND_Proof.
  intuition. *)
- eapply jnd_boxE.
  + apply IHJND_Proof1.
    intuition.
  + rewrite ctx_snoc_conc.
    apply IHJND_Proof2. 
    reflexivity.
- eapply jnd_boxEp.
  + apply IHJND_Proof1; intuition.
  + rewrite ctx_snoc_conc.
  eapply IHJND_Proof2; intuition.
Qed.

Global Hint Resolve jnd_weakening_vhyps : CS4.


Lemma jnd_weak_lastV : 
  forall (D: ctx) (G: ctx) (A: Judgm),
  JND_Proof D G A -> forall (B: Formula), JND_Proof (D, B) G A.
Proof.
intros.
assert (JND_Proof ((D,B);empty) G A); eauto with CS4.
Qed.

Global Hint Resolve jnd_weak_lastV : CS4.

(** Inversion introduction rules  *)
(* Detachment *)
Lemma jnd_intro_inv: 
  forall (D G:ctx) (A B:Formula), 
  JND_Proof D G (JTrue (A ==> B)) -> JND_Proof D (G,A) (JTrue B).
Proof.
intros.
eapply jnd_apply with A.
- change (G,A) with ((G,A);empty).
  apply jnd_weakening_thyps.
  assumption.
- intuition.
Qed.

Global Hint Resolve jnd_intro_inv : CS4.

Lemma jnd_introDia_inv:
  forall (D G: ctx) (A: Formula),
  JND_Proof D G (JTrue ($ A)) -> JND_Proof D G (JPoss A).
Proof.
intros.
eapply jnd_diaE in H.
exact H.
apply jnd_tp.
apply jnd_elem_thyps; intuition.
Qed.

Global Hint Resolve jnd_introDia_inv : CS4.

(* Detachment for boxed formulas *)
Lemma jnd_intro_val_inv: 
  forall (D G: ctx) (A B: Formula),
  JND_Proof D G (JTrue (#A ==> B)) -> JND_Proof (D,A) G (JTrue B).
Proof.
intros.
eapply jnd_apply.
apply jnd_weak_lastV.
exact H.
apply jnd_boxI.
change (D,A) with ((D,A);empty).
apply jnd_vhyp.
Qed.

Global Hint Resolve jnd_intro_val_inv : CS4.



(** Context Weakening *)

Lemma jnd_weakening_thyps_ctxR: 
  forall (D: ctx) (G : ctx) (A: Judgm), 
  JND_Proof D G A -> forall (G': ctx), JND_Proof D (G;G') A.
Proof.
intros.
induction G'; auto.
eapply jnd_weak_last in IHG'.
exact IHG'.
Qed.

Global Hint Resolve jnd_weakening_thyps_ctxR : CS4.

Lemma jnd_weakening_thyps_ctxL:
  forall (G: ctx) (D : ctx) (A: Judgm), 
  JND_Proof D G A -> forall (G': ctx), JND_Proof D (G';G) A.
Proof.
intro.
induction G.
- intros.
  simpl.
  rewrite <- (ctx_empty_conc G').
  apply jnd_weakening_thyps_ctxR.
  assumption.
- intros.
  simpl.
  dependent induction H; eauto with CS4.
  apply jnd_intro_inv.
  apply IHG.
  apply jnd_intro.
  rewrite <- x.
  intuition.
Qed.

Global Hint Resolve jnd_weakening_thyps_ctxL : CS4.



Lemma jnd_weakening_vhyps_ctxR:
  forall (D: ctx) (G : ctx) (A: Judgm), 
  JND_Proof D G A -> forall (D': ctx), JND_Proof (D;D') G A.
Proof.
intros.
induction D'; auto.
assert (JND_Proof ((D;D'),f) G A); auto with CS4.
Qed.

Global Hint Resolve jnd_weakening_vhyps_ctxR : CS4.


Lemma jnd_weakening_vhyps_ctxL:
  forall (D: ctx) (G : ctx) (A: Judgm), 
  JND_Proof D G A -> forall (D': ctx), JND_Proof (D';D) G A.
Proof.
intro.
induction D'; intros; auto with CS4.
rewrite ctx_empty_conc.
assumption.
Qed.

Global Hint Resolve jnd_weakening_vhyps_ctxL : CS4.



(** Exchange true hypotheses *)

Lemma jnd_exchange_thyps_True: 
  forall (D G:ctx) (A B C: Formula),
  JND_Proof D ((G,A),B) (JTrue C) -> JND_Proof D ((G,B),A) (JTrue C).
Proof.
intros.
apply (jnd_apply _ _ B C).
- apply jnd_intro.
  change (((G, B), A), B) with ((G, B);((empty, A), B)).
  apply jnd_weakening_thyps; intuition.
- intuition.
Qed.

Global Hint Resolve jnd_exchange_thyps_True : CS4.


Lemma jnd_exchange_thyps_Poss: 
  forall (D G:ctx) (A B C: Formula),
  JND_Proof D ((G,A),B) (JPoss C) -> JND_Proof D ((G,B),A) (JPoss C).
Proof.
intros.
dependent induction H.
- eapply jnd_boxEp.
  + apply jnd_exchange_thyps_True in H.
    exact H.
  + apply IHJND_Proof2; intuition.
- eapply jnd_diaE.
  + apply jnd_exchange_thyps_True in H.
    exact H.
  + exact H0.
- apply jnd_exchange_thyps_True in H.
  apply jnd_tp.
  assumption.
Qed.

Global Hint Resolve jnd_exchange_thyps_Poss : CS4.

Lemma jnd_exchange_thyps: 
  forall (C: Judgm) (D G:ctx) (A B: Formula),
  JND_Proof D ((G,A),B) C -> JND_Proof D ((G,B),A) C.
Proof.
intro.
induction C;  intros.
- apply jnd_exchange_thyps_True in H; intuition.
- apply jnd_exchange_thyps_Poss in H; intuition. 
Qed.

Global Hint Resolve jnd_exchange_thyps : CS4.

(** Context exchange for true hypotheses *)

Lemma jnd_exch_thyps_conc_True: 
  forall (G' D G:ctx) (A B: Formula),
   JND_Proof D ((G,A);G') (JTrue B) -> JND_Proof D (G;(G',A)) (JTrue B).
Proof.
intro.
induction G'.
- auto.
- intros.
  simpl in H.
  apply jnd_intro in H.
  apply IHG' in H.
  apply jnd_intro_inv in H.
  change (G; ((G', f), A)) with (((G; G'), f), A).
  change ((G; (G', A)), f) with (((G; G'), A), f) in H.
  apply jnd_exchange_thyps_True.
  assumption.
Qed.

Global Hint Resolve jnd_exch_thyps_conc_True : CS4.

Lemma jnd_exch_thyps_conc_Poss: 
  forall (G' D G:ctx) (A B: Formula),
   JND_Proof D ((G,A);G') (JPoss B) -> JND_Proof D (G;(G',A)) (JPoss B).
Proof.
intros.
dependent induction H.
- eapply jnd_boxEp.
  + apply jnd_exch_thyps_conc_True in H.
    exact H.
  + apply IHJND_Proof2; intuition.
- eapply jnd_diaE.
  + apply jnd_exch_thyps_conc_True in H.
    exact H.
  + exact H0.
- apply jnd_exch_thyps_conc_True in H.
  auto with CS4.
Qed.

Global Hint Resolve jnd_exch_thyps_conc_Poss : CS4.

Lemma jnd_exch_thyps_conc: 
  forall (B: Judgm) (G' D G:ctx) (A: Formula),
   JND_Proof D ((G,A);G') B -> JND_Proof D (G;(G',A)) B.
Proof.
intros.
case_eq B; intros; rewrite H0 in H.
apply jnd_exch_thyps_conc_True; intuition.
apply jnd_exch_thyps_conc_Poss; intuition.
Qed.

Global Hint Resolve jnd_exch_thyps_conc : CS4.

Lemma jnd_exch_thyps_snoc_True: 
 forall (G' D G:ctx) (A B: Formula),
 JND_Proof D (G;(G',A)) (JTrue B) -> JND_Proof D ((G,A);G') (JTrue B).
Proof.
intro.
induction G'.
- auto.
- intros.
  simpl.
  apply jnd_intro_inv.
  apply IHG'.
  simpl.
  apply jnd_intro.
  apply jnd_exchange_thyps_True.
  simpl in H.
  assumption.
Qed.

Global Hint Resolve jnd_exch_thyps_snoc_True : CS4.


Lemma jnd_exch_thyps_snoc_Poss: 
 forall (G' D G:ctx) (A B: Formula),
 JND_Proof D (G;(G',A)) (JPoss B) -> JND_Proof D ((G,A);G') (JPoss B).
Proof.
intros.
dependent induction H; eauto with CS4.
Qed.

Global Hint Resolve jnd_exch_thyps_snoc_Poss : CS4.


Lemma jnd_exch_thyps_snoc: 
 forall (B: Judgm) (G' D G:ctx) (A: Formula) ,
 JND_Proof D (G;(G',A)) B -> JND_Proof D ((G,A);G') B.
Proof.
intro.
case B; intros.
- apply jnd_exch_thyps_snoc_True; intuition.
- apply jnd_exch_thyps_snoc_Poss; intuition.
Qed.

Global Hint Resolve jnd_exch_thyps_snoc : CS4.


(** Generalized Inversion introduction rules *)

Lemma jnd_intro_gen: 
  forall (G' D G:ctx) (A B:Formula),
   JND_Proof D (G,A;G') (JTrue B) -> JND_Proof D (G;G') (JTrue (A ==> B)).
Proof.
intro.
induction G'.
- intros. auto with CS4.
- intros. 
  simpl in H.
  apply jnd_intro.
  apply jnd_exch_thyps_conc_True.
  assumption.
Qed.

Global Hint Resolve jnd_intro_gen : CS4.


Lemma jnd_intro_gen_inv: 
  forall (G' D G:ctx) (A B:Formula),
  JND_Proof D (G;G') (JTrue (A ==> B)) -> JND_Proof D (G,A;G') (JTrue B).
Proof.
intros.
apply jnd_intro_inv in H.
eauto with CS4. 
Qed.

Global Hint Resolve jnd_intro_gen_inv : CS4.



(** Substitutions *)

Theorem jnd_subst_1:
  forall (D G: ctx) (A: Formula), (JND_Proof D G (JTrue A)) ->
  forall (G':ctx) (C: Formula),
    (JND_Proof D ((G,A);G') (JTrue C)) -> JND_Proof D (G;G') (JTrue C).
Proof.
intros.
apply jnd_intro_gen in H0.
eapply jnd_apply.
exact H0.
apply jnd_weakening_thyps_ctxR.
assumption.
Qed.

Global Hint Resolve jnd_subst_1 : CS4.

Theorem jnd_subst_2:
  forall (D G: ctx) (A: Formula), (JND_Proof D G (JTrue A)) ->
  forall (G':ctx) (C: Formula),
    (JND_Proof D ((G,A);G') (JPoss C)) -> JND_Proof D (G;G') (JPoss C).
Proof.
intros.
dependent induction H0. (* ; eauto. *)
- eapply jnd_boxEp.
  + eapply jnd_apply.
    -- apply jnd_intro_gen.
       exact H0_.
    -- apply jnd_weakening_thyps_ctxR.
       assumption.
  + eapply IHJND_Proof2.
    -- eapply jnd_weak_lastV.
       exact H.
    -- intuition.
    -- intuition.
- eapply jnd_diaE.
  + eapply jnd_apply.
    -- apply jnd_intro_gen.
       exact H0_.
    -- apply jnd_weakening_thyps_ctxR.
       exact H.
  + exact H0_0.
- eauto with CS4. 
Qed.

Global Hint Resolve jnd_subst_2 : CS4.

Theorem jnd_subst_5:
  forall (D G: ctx) (A: Formula), (JND_Proof D G (JPoss A)) ->
  forall (C: Formula),
    (JND_Proof D (empty, A) (JPoss C)) -> JND_Proof D G (JPoss C).
Proof.
(* Direct proof, subst5 is consequence of diaE, eauto ok *)
intros.
eapply jnd_diaE.
- apply jnd_diaI.
  exact H.
- exact H0.
Qed.


(** 
  Transference relationship of formulas from one context to another *)


(* Valid formulas are necessary truths *)
Proposition val_to_true: 
  forall (G D:ctx) (A B: Formula),
   JND_Proof (D,A) G (JTrue B) -> JND_Proof D (G, #A) (JTrue B).
Proof.
intros.
remember (jnd_boxE D (G, # A) A B) as n.
apply n.
intuition.
apply jnd_weak_last.
exact H.
Qed.

Global Hint Resolve val_to_true : CS4.


Corollary ctx_val_to_true:
  forall (D G: ctx) (A: Formula),
  JND_Proof D G (JTrue A) -> JND_Proof empty (boxed D; G) (JTrue A).
Proof.
intro.
induction D; auto with CS4.
intros.
simpl.
apply val_to_true in H.
apply IHD in H.
simpl in H.
apply jnd_exch_thyps_snoc_True.
assumption.
Qed.

Global Hint Resolve ctx_val_to_true : CS4.





Lemma jnd_intro_val: 
  forall (D G: ctx) (A B: Formula),
  JND_Proof (D,A) G (JTrue B) -> JND_Proof D G (JTrue (#A ==> B)).
Proof.
intros.
auto with CS4.
Qed.

Global Hint Resolve jnd_intro_val: CS4.



(** Exchange valid hypotheses *)

Lemma jnd_exchange_vhyps_True: 
  forall (D G:ctx) (A B C: Formula),
  JND_Proof ((D,A),B) G (JTrue C) -> JND_Proof ((D,B),A) G (JTrue C).
Proof.
intros.
apply (jnd_apply _ _ (# B) C).
- apply jnd_intro.
  eapply jnd_boxE.
  + change (G, (#B)) with ((G,(#B));empty).
     eapply jnd_thyp.
  + apply jnd_weak_last.
  change (((D, B), A), B) with ((D, B);((empty, A), B)).
  apply jnd_weakening_vhyps.
  intuition.
- intuition.
Qed.

Global Hint Resolve jnd_exchange_vhyps_True : CS4.


Lemma jnd_exchange_vhyps_Poss: 
  forall (D G:ctx) (A B C: Formula),
  JND_Proof ((D,A),B) G (JPoss C) -> JND_Proof ((D,B),A) G (JPoss C).
Proof.
intros.
dependent induction H. (*; eauto.*) 
- apply jnd_diaI in H0.
  apply jnd_intro_val in H0.
  apply jnd_exchange_vhyps_True in H0.
  apply jnd_intro_val_inv in H0.
  apply jnd_introDia_inv in H0.
  apply jnd_exchange_vhyps_True in H.
  apply (jnd_boxEp ((D,B),A) G A0 C H) in H0.
  exact H0.
- apply (jnd_diaE ((D,A),B) G A0 C H) in H0.
  apply jnd_diaI in H0.
  apply jnd_exchange_vhyps_True in H0.
  apply jnd_introDia_inv in H0.
  exact H0.
- apply jnd_exchange_vhyps_True in H.
  apply jnd_tp.
  exact H.
Qed.

Global Hint Resolve jnd_exchange_vhyps_Poss : CS4.


(* Corollary 9 Implication introduction for enhanced formula *)
Corollary jnd_intro_val_gen:
  forall (D D' G: ctx) (A B: Formula),
  JND_Proof ((D,A);D') G (JTrue B) -> JND_Proof (D;D') G (JTrue (#A ==> B)).
Proof.
intros.
dependent induction D'; auto with CS4.

apply jnd_intro_val.
simpl.
apply jnd_exchange_vhyps_True.
simpl in H.
 
eapply val_to_true in H.
eapply jnd_intro in H.
apply IHD' in H.
repeat apply jnd_intro_val_inv.
assumption.
Qed.

Global Hint Resolve jnd_intro_val_gen : CS4.





Theorem jnd_subst_3:
  forall (D: ctx) (B: Formula), (JND_Proof D empty (JTrue B)) ->
  forall (D' G:ctx) (C: Formula),
    (JND_Proof ((D,B);D') G (JTrue C)) -> JND_Proof (D;D') G (JTrue C).
Proof.
intros.
eapply jnd_apply.
- eapply (jnd_boxI D G) in H.
  apply jnd_intro_val_gen in H0.
  exact H0.
- apply jnd_weakening_vhyps_ctxR.
  auto with CS4.
Qed.

Global Hint Resolve jnd_subst_3 : CS4.


Proposition val_to_true_Poss: 
  forall (G D:ctx) (A B: Formula),
   JND_Proof (D,A) G (JPoss B) -> JND_Proof D (G, #A) (JPoss B).
Proof.
intros. 
dependent induction H; auto with CS4; intros.
- eapply jnd_boxEp.
 + apply val_to_true in H.
   apply H.
 + apply jnd_diaI in H0.
   apply jnd_intro_val in H0.
   apply val_to_true in H0.
   apply jnd_intro_val_inv in H0.
   apply jnd_introDia_inv in H0.
   exact H0.
- apply jnd_introDia_inv.
  apply val_to_true.
  apply (jnd_diaE (D,A) G A0 B H) in H0.
  intuition.
Qed.

Global Hint Resolve val_to_true_Poss : CS4.


Corollary ctx_val_to_true_Poss:
  forall (D G: ctx) (A: Formula),
  JND_Proof D G (JPoss A) -> JND_Proof empty (boxed D; G) (JPoss A).
Proof.
intro.
induction D.
- intros; intuition.
- intros.
simpl.
apply val_to_true_Poss in H. 
apply IHD in H.
simpl in H.
apply jnd_exch_thyps_snoc_Poss.
assumption.
Qed.

Global Hint Resolve ctx_val_to_true_Poss : CS4.


Corollary ctx_true_to_val:
  forall (D G: ctx) (A: Formula),
  JND_Proof empty (boxed D; G) (JTrue A) -> JND_Proof D G (JTrue A).
Proof.
intro.
induction D.
- simpl.
  intros.
  rewrite (ctx_empty_conc G) in H.
  assumption.
- intros.
  simpl in H.
  apply jnd_intro_gen in H.
  apply IHD in H.
  apply jnd_intro_val_inv in H.
  assumption.
Qed.

Global Hint Resolve ctx_true_to_val : CS4.

(* Proposition 11 *)
(* Neccesary truths are valid  *)
Proposition true_to_val_True: 
  forall (D G: ctx) (A B: Formula),
    JND_Proof D (G, #A) (JTrue B) -> JND_Proof (D,A) G (JTrue B).
Proof.
intros.
apply jnd_intro_val_inv.
apply jnd_intro.
assumption.
Qed.

Global Hint Resolve true_to_val_True : CS4.


(* Proposition 10 *)
Lemma jnd_intro_val_gen_inv:
  forall (D D' G: ctx) (A B: Formula),
  JND_Proof (D;D') G (JTrue (#A ==> B)) -> JND_Proof ((D,A);D') G (JTrue B).
Proof.
intros D D' G A.
induction D'; simpl.
- intuition.
- intros.
  eapply jnd_intro_inv in H.
  intuition.
(*   apply true_to_val_True in H.
  simpl in H.
  apply jnd_exchange_vhyps_True in H.
  repeat (apply jnd_intro_val in H).
  apply IHD' in H.
  apply jnd_intro_val_inv in H.
  simpl.
  intuition.
 *)
Qed.

Global Hint Resolve jnd_intro_val_gen_inv : CS4.


(** Context Exchange valid hypotheses *)

Lemma jnd_exch_vhyps_snoc_True: 
 forall (D' D G:ctx) (A B: Formula),
 JND_Proof (D;(D',A)) G (JTrue B) -> JND_Proof ((D,A);D') G (JTrue B).
Proof.
intros.
dependent induction D'; auto with CS4.
(* simpl in H.
apply jnd_intro_val in H.
change ((D; D'), f) with (D; (D', f)) in H.
apply jnd_intro_val_gen_inv in H. 
assumption.
 *)
Qed.

Global Hint Resolve jnd_exch_vhyps_snoc_True : CS4.



Lemma jnd_exch_vhyps_snoc_Poss: 
 forall (D' D G:ctx) (A B: Formula),
 JND_Proof (D;(D',A)) G (JPoss B) -> JND_Proof ((D,A);D') G (JPoss B).
Proof.
intro.
induction D'.
- intros.
  intuition.
- intros.
simpl in H.
apply jnd_exchange_vhyps_Poss in H.
apply jnd_diaI in H.
repeat (apply jnd_intro_val in H).
apply jnd_intro_val_gen_inv in H.
apply jnd_intro_val_inv in H.
apply jnd_introDia_inv in H.
assumption.
Qed.

Global Hint Resolve jnd_exch_vhyps_snoc_Poss : CS4.

Lemma jnd_exch_vhyps_conc_True: 
 forall (D' D G:ctx) (A B: Formula),
 JND_Proof ((D,A);D') G (JTrue B) -> JND_Proof (D;(D',A)) G (JTrue B).
Proof.
intros.
simpl.
apply true_to_val_True.
apply jnd_intro_val_gen in H.
apply jnd_intro_inv.
assumption.
Qed.

Global Hint Resolve jnd_exch_vhyps_conc_True : CS4.

Lemma jnd_exch_vhyps_conc_Poss: 
 forall (D' D G:ctx) (A B: Formula),
 JND_Proof ((D,A);D') G (JPoss B) -> JND_Proof (D;(D',A)) G (JPoss B).
Proof.
intros.
simpl.
apply jnd_introDia_inv.
apply jnd_diaI in H.
apply jnd_intro_val_gen in H.
apply jnd_intro_val_inv.
assumption.
Qed.

Global Hint Resolve jnd_exch_vhyps_conc_Poss : CS4.

Theorem jnd_subst_4:
  forall (D: ctx) (B: Formula), (JND_Proof D empty (JTrue B)) ->
  forall (D' G:ctx) (C: Formula),
    (JND_Proof ((D,B);D') G (JPoss C)) -> JND_Proof (D;D') G (JPoss C).
Proof.
intros.
dependent induction H0. (*; eauto. *)
- eapply jnd_boxEp.
  + eapply jnd_weakening_vhyps_ctxR in H.
    eapply jnd_boxI in H.
    exact H.
  + eapply (jnd_boxEp _ _ _ _ H0_) in H0_0.
    eapply jnd_exch_vhyps_conc_Poss in H0_0. 
    simpl in H0_0.
    assumption.
- eapply (jnd_diaE _ _ _ _ H0_) in H0_0.
  eapply jnd_boxEp.
  + eapply jnd_weakening_vhyps_ctxR.
    eapply jnd_boxI in H.
    exact H.
  + eapply jnd_exch_vhyps_conc_Poss.
    assumption.
- eauto with CS4.
Qed.

Global Hint Resolve jnd_subst_4 : CS4.



(** Proposition 11 *) 
Proposition true_to_val_Poss: 
  forall (D G: ctx) (A B: Formula),
    JND_Proof D (G, #A) (JPoss B) -> JND_Proof (D,A) G (JPoss B).
Proof.
intros.
dependent induction H. (*; eauto.*)
- eapply jnd_boxEp.
  + apply true_to_val_True in H.
    exact H.
  + apply jnd_exchange_vhyps_Poss.
    apply IHJND_Proof2; intuition.
- apply (jnd_diaE D (G, (#A)) A0 B H) in H0.
  apply jnd_diaI in H0.
  apply jnd_intro in H0.
  apply jnd_intro_val_inv in H0.
  apply jnd_introDia_inv in H0.
  assumption.
- apply true_to_val_True in H.
  apply jnd_tp in H.
  assumption.
Qed.

Global Hint Resolve true_to_val_Poss : CS4.


(** Proposition 12 Transference from enhanced to enhanced *)
Proposition box_enhanced: 
  forall (D D' G:ctx) (A: Formula) (J : Judgm),
   JND_Proof ((D,A);D') G J -> JND_Proof ((D, #A);D') G J.
Proof.
intros D D' G A J.
induction J.
intro.
apply (jnd_boxE _ _ (A)); intuition.
intro.
apply (jnd_boxEp _ _ (A)); intuition.
change (((D, (# A)); D'), A) with ((D, (# A));(D', A)).
apply jnd_weakening_vhyps; intuition.
Qed.

Global Hint Resolve box_enhanced : CS4.




(** 
 Axioms that characterize S4 are derivable in CS4
 *)
 
Lemma AxI_CS: 
  forall (D G:ctx) (A:Formula), JND_Proof D G (JTrue (A ==> A)).
Proof.
intros.
apply jnd_intro.
apply jnd_elem_thyps.
intuition.
Qed.

Global Hint Resolve AxI_CS : CS4.


Lemma Ax1_CS: 
  forall (D G:ctx) (A B:Formula), JND_Proof D G (JTrue (A ==> (B ==> A))).
Proof.
intros.
repeat apply jnd_intro.
intuition.
Qed.

Global Hint Resolve Ax1_CS : CS4.


Lemma Ax2_CS: 
  forall (D G:ctx) (A B:Formula), 
   JND_Proof D G (JTrue ((A ==> (A ==> B)) ==> (A ==> B))).
Proof.
intros.
repeat apply jnd_intro.
eapply (jnd_apply D _ A B).
- eapply (jnd_apply D _ A (A ==> B)); intuition.
- apply jnd_elem_thyps; intuition.
Qed.

Global Hint Resolve Ax2_CS : CS4.


Lemma Ax3_CS: 
  forall (D G:ctx) (A B C:Formula), 
   JND_Proof D G (JTrue ((A ==> (B ==> C)) ==> (B ==> (A ==> C)))).
Proof.
intros.
repeat apply jnd_intro.
eapply jnd_apply.
 - eapply (jnd_apply _ _ A (B ==> C)).
  + auto with CS4.
  + intuition.
- apply (jnd_elem_thyps _ _ B).
  auto with context.
Qed.

Global Hint Resolve Ax3_CS : CS4.

Lemma Ax4_CS: 
  forall (D G:ctx) (A B C:Formula), 
   JND_Proof D G (JTrue ((B ==> C) ==> ((A ==> B) ==> (A ==> C)))).
Proof.
intros.
repeat apply jnd_intro.
eapply jnd_apply.
- apply jnd_elem_thyps; intuition.
- apply (jnd_apply _ _ A B); intuition.
Qed.

Global Hint Resolve Ax4_CS : CS4. 

 

Theorem Axiom_T: 
  forall (D G: ctx) (A:Formula), JND_Proof D G (JTrue ((#A) ==> A)).
Proof.
intros.
apply jnd_intro.
eapply jnd_boxE.
auto with CS4.
intuition.
Qed.

Global Hint Resolve Axiom_T : CS4.


Theorem Axiom_4: 
  forall (D G: ctx) (A:Formula), JND_Proof D G (JTrue (#A ==> ##A)).
Proof.
intros.
apply jnd_intro.
eapply jnd_boxE. 
- apply (jnd_elem_thyps _ _ (#A)). 
  intuition.
- intuition.
Qed.

Global Hint Resolve Axiom_4 : CS4.


Theorem Axiom_K: 
  forall (D G: ctx) (A B:Formula),
  JND_Proof D G (JTrue ((#(A ==> B)) ==> ((#A) ==> (#B)))).
Proof.
intros.
repeat (apply jnd_intro).
apply (jnd_boxE D ((G, (# (A ==> B))), (# A)) A (#B)).
- intuition. 
- apply (jnd_boxE _ _ (A ==> B) (#B)).
  * intuition.
  * apply jnd_boxI.
    apply (jnd_apply _ _ A B); intuition. 
Qed.

Global Hint Resolve Axiom_K : CS4.

Theorem Axiom_D:
  forall (D G: ctx) (A: Formula),
  JND_Proof D G (JTrue (#A ==> $A)).
Proof.
intros.
(* eauto. *)
apply jnd_intro_val.
apply jnd_diaI.
apply jnd_tp.
rewrite <- (ctx_conc_empty (D,A)).
apply jnd_vhyp.
Qed.

Global Hint Resolve Axiom_D : CS4.


Theorem Axiom_DiaK:
  forall (D G: ctx) (A B: Formula),
  JND_Proof D G (JTrue ((#(A ==> B)) ==> ($A ==> $B))).
Proof.
intros.
apply jnd_intro_val.
apply jnd_intro.
apply jnd_diaI.
eapply jnd_diaE.
- rewrite <- (ctx_conc_empty (G,($A))). apply jnd_thyp.
- apply jnd_tp.
  apply (jnd_apply _ _ A B).
  + apply jnd_elem_vhyps; intuition.
  + apply jnd_elem_thyps; intuition.
Qed.

Global Hint Resolve Axiom_DiaK : CS4.


Theorem Axiom_Dia4:
  forall (D G: ctx) (A: Formula),
  JND_Proof D G (JTrue ($$A ==> $A)).
Proof.
intros.
apply jnd_intro.
apply jnd_diaI.
eapply jnd_diaE.
- 
rewrite <- (ctx_conc_empty (G,$$A)).
apply jnd_thyp.
-
apply (jnd_diaE _ _ A A).
+ rewrite <- (ctx_conc_empty (empty,$A)).
  apply jnd_thyp.
+ apply jnd_tp. auto with CS4. 
Qed.

Global Hint Resolve Axiom_Dia4 : CS4.


Theorem Axiom_DiaT:
  forall (D G: ctx) (A: Formula),
  JND_Proof D G (JTrue (A ==> $A)).
Proof.
intros.
apply jnd_intro.
apply jnd_diaI.
apply jnd_tp.
rewrite <- (ctx_conc_empty (G,A)).
apply jnd_thyp.
Qed.

Global Hint Resolve Axiom_DiaT : CS4.



