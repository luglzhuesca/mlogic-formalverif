(** Hilbert style System HK4 from
      Sara Negri & Raul Hakli, 
      Does the deduction theorem fail for modal logic?,
      Synthese 187 (2012), pp.849--867.
*)

Require Import Coq.Program.Equality.
Require Import ModalLogic.
Require Import Omega.
Require Import Context.

(** --------------- INFERENCE RULES --------------- *)
(** System HK for modal logic with local hypotheses, 
  without negation but extended with modal axioms for T and 4
  and the corresponding versions for the possibility modality.
  As remarked by Hakli and Negri [1],
  in the original system there is no substitution rule, 
  the axiom schemata is used to perform implicit substitutions 
  giving instances of axioms whenever it is needed.
*)


Inductive Deriv: ctx -> Formula -> Prop:=
| Hyp:    forall (G : ctx) (A: Formula), 
          elem A G -> Deriv G A 
      
| Ax1:    forall (G: ctx) (A B: Formula), 
          Deriv G (A ==> (B ==> A))
       
| Ax2:    forall (G: ctx) (A B: Formula), 
          Deriv G ((A ==> (A ==> B)) ==> (A ==> B))
       
| Ax3:    forall (G: ctx) (A B C: Formula), 
          Deriv G ((A ==> (B ==> C)) ==> (B ==>(A ==> C)))
       
| Ax4:    forall (G: ctx) (A B C: Formula), 
          Deriv G ((B ==> C) ==> ((A ==> B) ==> (A ==> C)))
       
| AxBoxK: forall (G: ctx) (A B: Formula), 
          Deriv G (Box(A ==> B) ==> ((Box A) ==> (Box B)))
        
| AxBoxT: forall (G : ctx) (A : Formula),
          Deriv G ((Box A) ==> A)
       
| AxBox4: forall (G : ctx) (A : Formula),
          Deriv G ((Box A) ==> (Box(Box A)))
       
| AxDiaT: forall (G: ctx) (A: Formula),
          Deriv G (A ==> Dia A)

| AxDiaK: forall (G: ctx) (A B: Formula),
          Deriv G (Box (A ==> B) ==> (Dia A ==> Dia B))

| AxDia4: forall (G: ctx) (A: Formula),
          Deriv G (Dia (Dia A) ==> Dia A)

| MP:     forall (G G': ctx) (A B: Formula),
          Deriv G A -> Deriv G' (A ==> B) -> Deriv (G';G) B
      
| Nec:    forall (G: ctx) (A: Formula), 
          Deriv empty A -> Deriv G (Box A)
(* 
| Pos:    forall (G: ctx) (A: Formula), 
          Deriv G A -> Deriv G (Dia A) *).
          
Global Hint Constructors Deriv : HK4.

Notation "G |- A" := (Deriv G A) (at level 30).


(** 
 Verification of statements in the article and 
 in addition other useful lemmas, 
 some of them are the dettached versions of the axioms
 *)

Lemma AxI: 
  forall (G:ctx) (A:Formula), G |- (A ==> A).
Proof.
intros.
assert (H := Ax1 empty A A).
assert (H1:= Ax2 G A A).
rewrite <- (ctx_conc_empty G).
eapply MP in H1.
- exact H1.
- exact H.
Qed.

Global Hint Resolve AxI : HK4.


Lemma Ax3_dett: 
  forall (G:ctx) (A B C:Formula),
  G |- (A ==> B ==> C) -> G |- (B ==> A ==> C).
Proof.
intros.
rewrite <- (ctx_empty_conc G).
eapply MP.
- exact H.
- apply Ax3.
Qed.

Global Hint Resolve Ax3_dett : HK4.


(* Theorem 4.2 Deduction *)
Theorem DeductionTh: 
  forall (G: ctx) (A B: Formula), (G,A) |- B -> G |- (A ==> B).
Proof.
intros G A B H.
dependent induction H; rewrite <- (ctx_empty_conc G).
(* ; eauto. *) 
(* the eauto tactic solves all axiom instances together with Nec rule
   and leaves to the user the cases of hypothesis and MP *)
- apply elem_inv in H.
  destruct H.
  + rewrite H.
    apply AxI.
  + eapply (MP _ _ A0 (A==> A0)); intuition.
- eapply (MP _ _ (A0 ==> B ==> A0) _); apply Ax1.
- eapply (MP _ _ ((A0 ==> A0 ==> B) ==> A0 ==> B)).
  apply Ax2.
  apply Ax1.
- eapply (MP _ _ ((A0 ==> B ==> C) ==> B ==> A0 ==> C) _).
  apply Ax3.
  apply Ax1.
- eapply (MP _ _ ((B ==> C) ==>(A0 ==> B) ==> A0 ==> C) _ ).
  apply Ax4.
  apply Ax1.
- eapply (MP _ _ (# (A0 ==> B) ==> # A0 ==> # B) _).
  apply AxBoxK.
  apply Ax1.
- eapply (MP _ _ (# A0 ==> A0) _).
  apply AxBoxT.
  apply Ax1.
- eapply (MP _ _ (# A0 ==> # # A0) _).
  apply AxBox4.
  apply Ax1.
- eapply (MP _ _ (A0 ==> $A0) _).
  apply AxDiaT.
  apply Ax1.  
- eapply (MP _ _ (# (A0 ==> B) ==> $ A0 ==> $ B) _).
  apply AxDiaK.
  apply Ax1.
- eapply (MP _ _ ($$ A0 ==> $A0) _).
  apply AxDia4.
  apply Ax1.
- rewrite ctx_empty_conc.
  assert(X:=x).
  apply ctx_decomposition in x.
  destruct x.
  + destruct H1.
    rewrite H2 in H0.
    apply IHDeriv2 in H2.
    apply Ax3_dett in H2.
    rewrite H1 in H.
    rewrite <- (ctx_conc_empty G).
    eapply MP.
    -- exact H.
    -- assumption.
  + destruct H1.
    assert (W := H1).
    apply IHDeriv1 in H1.
    rewrite W in X.
    simpl in X.
    inversion X.
    assert (empty |- ((A0 ==> B) ==> (A ==>A0) ==> (A ==> B))); intuition.
    assert (G' |- ((A ==> A0) ==> A ==> B)).
    -- eapply MP in H2.
       2: exact H0.
       rewrite (ctx_empty_conc G') in H2.
       exact H2.
    -- eapply MP.
       exact H1.
       exact H4.
    (* this subproof leaves an unsolved and unfocused goal !!!
    eapply (MP _ _ (A ==>A0) (A ==> B)).
    -- exact H1.
    -- rewrite <- (ctx_empty_conc G').
       eapply (MP G' empty (A0 ==> B) ((A ==> A0) ==> A ==> B) _).
       apply AxB. *)
- eapply MP. 
  + apply Nec.
    exact H.
  + apply Ax1.
(* - assert (G |- (A ==> A0)); intuition.
  assert (empty |- (A0 ==> $A0)); intuition.
  assert (empty |- ((A0 ==> $ A0) ==> (A ==> A0) ==> (A ==> $ A0))); intuition.
  eapply (MP _ _ _ _ H1) in H2.
  eapply (MP _ _ _ _ H0) in H2.
 simpl in H2.
assumption. *)
Qed.

Global Hint Resolve DeductionTh : HK4.


(* Corollary 4.3  Multiple Discharge*)
Corollary multihyp_discharge:
  forall (n:nat) (G: ctx) (A B: Formula),
  G;(replicate A n) |- B -> G |- (A==>B).
Proof.
intros.
dependent induction n; simpl in H ; rewrite <- (ctx_empty_conc G). 
- eapply (MP _ _ B (A ==> B)).
  + assumption.
  + apply Ax1.
- apply DeductionTh in H.
  apply IHn in H.
  eauto with HK4.
Qed.

Global Hint Resolve multihyp_discharge : HK4.


(* Corollary 4.4 Substitution or closure under composition *)
Corollary substitution:
  forall (G G': ctx) (A B: Formula),
  (G |- A) -> (G',A |- B) -> (G';G |- B).
Proof.
intros.
apply DeductionTh in H0.
eapply MP.
- exact H.
- exact H0.
Qed.

Global Hint Resolve substitution : HK4.


(* Theorem 4.5 Inverse Deduction Theorem *)
Theorem inverseDT:
  forall (G: ctx) (A B: Formula), G |- (A ==> B) -> G,A |- B.
Proof.
intros.
assert ((empty,A)|- A); intuition.
change (G,A) with (G; (empty,A)).
eapply MP.
exact H0.
exact H.
Qed.

Global Hint Resolve inverseDT : HK4.

(* Theorem 4.6 General Deduction Theorem*)
Theorem deductionTh_genPremise: 
  forall (G' G: ctx) (A B: Formula), (G,A);G' |- B -> G;G' |- (A ==> B).
Proof.
intro.
induction G' ; auto ; intros.
+ 
simpl in H.
apply DeductionTh in H.
intuition.
+
simpl  in H.
apply DeductionTh in H.
apply IHG' in H.
apply Ax3_dett in H.
apply inverseDT in H.
intuition.
Qed.

Global Hint Resolve deductionTh_genPremise : HK4.


(* Lemma 4.7 Context Permutation *)
Lemma ctx_permutation: 
  forall (G G':ctx) (A:Formula), G;G' |- A -> G';G |- A.
Proof.
intro G.
induction G.
- intros.
  simpl.
  rewrite ctx_empty_conc in H.
  assumption.
- intros.
  simpl.
  apply inverseDT.
  apply IHG.
  apply deductionTh_genPremise in H.
  assumption.
Qed.

Global Hint Resolve ctx_permutation : HK4.

Theorem inverseDT_genPremise:
  forall (G' G: ctx) (A B: Formula), G;G' |- (A ==> B) -> (G,A);G' |- B.
Proof.
intros.
apply ctx_permutation in H.
apply inverseDT in H.
apply ctx_permutation.
intuition.
Qed.

Global Hint Resolve inverseDT_genPremise : HK4.

Lemma ctx_permutation_gen: 
  forall (G'' G' G:ctx) (A:Formula), (G;G');G'' |- A -> (G';G);G'' |- A.
Proof.
intro.
induction G''; intros.
- simpl. simpl in H. apply ctx_permutation; intuition.
- simpl. apply inverseDT. 
  simpl in H. apply DeductionTh in H.
  apply IHG''; intuition.
Qed.

Global Hint Resolve ctx_permutation_gen : HK4.

Corollary contraction_hyp:
  forall (G : ctx) (A B : Formula), (G, A), A |- B -> G, A |- B.
Proof.
intros.
apply inverseDT.
repeat apply DeductionTh in H.
assert(K:= Ax2 empty A B).
rewrite <- (ctx_empty_conc G).
eapply MP.
exact H.
assumption.
Qed.

Global Hint Resolve contraction_hyp : HK4.


Lemma Ax2_dett: 
  forall (G:ctx) (A B:Formula), G |- (A ==> A ==> B) -> G |- (A ==> B).
Proof.
intros.
auto with HK4.
Qed.

Global Hint Resolve Ax2_dett : HK4.


(* Lemma 4.8 Context Contraction *)
Lemma ctx_contraction: 
  forall (G:ctx) (A:Formula), G;G |- A -> G |- A.
Proof.
intro.
induction G.
- auto.
- intros.
  apply deductionTh_genPremise in H.
  simpl in H.
  apply DeductionTh in H.
  apply Ax2_dett in H.
  apply inverseDT.
  apply IHG.
  assumption.
Qed.

Global Hint Resolve ctx_contraction : HK4.


Lemma ctx_contraction_gen: 
  forall (G G' G'':ctx) (A:Formula), G';(G;G);G'' |- A -> G';G;G'' |- A.
Proof.
intro.
induction G; intros; auto.

rewrite <- (ctx_snoc_conc G G f f) in H.
simpl in H.
apply deductionTh_genPremise in H.
rewrite (ctx_conc_conc G' (G, f) G) in H.
apply ctx_permutation_gen in H.
simpl in H.
apply deductionTh_genPremise in H.
apply ctx_permutation_gen in H.
rewrite <- (ctx_conc_conc G' G G) in H.

apply IHG in H.
apply Ax2_dett in H.
simpl.
apply inverseDT_genPremise.
intuition.
Qed.

Global Hint Resolve ctx_contraction_gen : HK4.


Lemma transitivity:
  forall (G: ctx) (A B C: Formula),
  G |- ((A ==> B) ==> ((B ==> C) ==> (A==> C))).
Proof.
intros.
rewrite <- (ctx_empty_conc G).
eapply MP.
2: eapply Ax3.
intuition.
Qed.

Global Hint Resolve transitivity : HK4.


Lemma trans_dett:
  forall (G G': ctx)(P Q R : Formula),
  G |- (P ==> Q) -> G' |- (Q ==> R) -> (G;G') |- (P ==> R).
Proof.
intros.
assert (K:= transitivity empty P Q R).
rewrite <- (ctx_empty_conc G).
eapply MP.
- exact H0.
- eapply MP.
  exact H.
  assumption.
Qed.

Global Hint Resolve trans_dett : HK4.


Lemma AxBoxK_dett: 
  forall (G:ctx) (A B:Formula), (G |- #(A ==> B)) -> G|- (#A ==> #B).
Proof.
intros.
rewrite <- (ctx_empty_conc G).
eapply MP.
- exact H.
- apply AxBoxK.
Qed.

Global Hint Resolve AxBoxK_dett : HK4.

Lemma AxDiaK_dett:
  forall (G: ctx) (A B: Formula), (G |- #(A ==> B)) -> G |- ($A ==> $B).
Proof.
intros.
eauto with HK4.
Qed.

Global Hint Resolve AxDiaK_dett : HK4.
